import {call, put, takeLatest} from "redux-saga/effects";
import api from "../api";
import {
    addLineToArticle,
    addLineToArticleSuccess,
    deleteLineFromArticle,
    editArticleLine,
    editArticleLineSuccess
} from "../actions-reducers";
import {deleteLineFromArticleSuccess} from "../actions-reducers/selectionDossier";

function* postArticleLinesSaga({type, payload}) {
    try {
        const {data} = yield call(api.post, `/api/articles/${payload.article.uuid}/lines`, payload.newLine);
        yield put(addLineToArticleSuccess(data));
    } catch (e) {

    }
}

function* putArticleLineSaga({type, payload}) {
    try {
        const {data} = yield call(api.put, `/api/articles/${payload.article.uuid}/lines/${payload.newLine.uuid}`, payload.newLine);
        yield put(editArticleLineSuccess({
            updatedArticle: data,
            updatedArticleLine: payload.newLine
        }));
    } catch (e) {

    }
}

function* deleteArticleLineSaga({type, payload}) {
    try {
        const {data} = yield call(api.delete, `/api/articles/${payload.articleUuid}/lines/${payload.lineUuid}`);
        yield put(deleteLineFromArticleSuccess(data));
    } catch (e) {

    }
}

export const articleLinesSaga = function* () {
    yield takeLatest(addLineToArticle, postArticleLinesSaga);
    yield takeLatest(editArticleLine, putArticleLineSaga);
    yield takeLatest(deleteLineFromArticle, deleteArticleLineSaga);
};