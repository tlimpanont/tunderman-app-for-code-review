import {call, put, takeLatest} from "redux-saga/effects";
import api from "../api";
import {
    addPictureToArticle,
    addPictureToArticleSuccess,
    removePictureFromArticle,
    removePictureFromArticleSuccess,
} from "../actions-reducers";

function* postArticlePhotoSaga({type, payload}) {
    try {
        const {data} = yield call(api.post, `/api/articles/${payload.uuid}/photos`, payload.dataURL);
        yield put(addPictureToArticleSuccess(data));
    } catch (e) {

    }
}

function* deleteArticlePhotoSaga({type, payload}) {
    try {
        const {data} = yield call(api.delete, payload);
        yield put(removePictureFromArticleSuccess(data));
    } catch (e) {

    }
}

export const articlePhotosSaga = function* () {
    yield takeLatest(addPictureToArticle, postArticlePhotoSaga);
    yield takeLatest(removePictureFromArticle, deleteArticlePhotoSaga);
};