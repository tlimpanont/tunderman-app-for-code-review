import {call, put, takeLatest} from "redux-saga/effects";
import api from "../api";
import {
    fetchFilteredDossiers,
    fetchFilteredDossiersSuccess,
    saveAndExportDossier, saveAndExportDossierSuccess,
    startUnloadingDossier,
    startUnloadingDossierSuccess,
} from "../actions-reducers";
import {fetchDossierSuccess, selectDossier} from "../actions-reducers/selectionDossier";

function* fetchFilteredDossiersSaga({payload}) {
    try {
        let url = `/api/dossiers?state=${payload.state}`;
        if (payload.startDate) {
            url += `&startDate=${payload.startDate}`;
        }
        if (payload.numberOfDays) {
            url += `&numberOfDays=${payload.numberOfDays}`;
        }
        const {data} = yield call(api.get, url);
        yield put(fetchFilteredDossiersSuccess(data));
    } catch (e) {
    }
}

function* fetchDossierSaga({type, payload}) {
    try {
        const {data} = yield call(api.get, `/api/dossiers/${payload}`);
        yield put(fetchDossierSuccess(data));
    } catch (e) {

    }
}

function* startUnloadingDossierSaga({type, payload}) {
    try {
        const requestBody = {
            uuid: payload.uuid,
            state: "DRAFT",
            unloadingStartDate: new Date(),
            dockNumber: payload.dockNumber
        };
        const {data} = yield call(api.patch, `/api/dossiers/${payload.uuid}`, requestBody);

        yield put(startUnloadingDossierSuccess({
            ...data,
            unloadingStartDate: requestBody.unloadingStartDate
        }));
    } catch (e) {

    }
}

function* saveAndExportDossierSaga({type, payload}) {
    try {
        const requestBody = {
            uuid: payload.uuid,
            state: "DONE",
            unloadingEndDate: new Date()
        };
        const {data} = yield call(api.patch, `/api/dossiers/${payload.uuid}`, requestBody);

        yield put(saveAndExportDossierSuccess(data));
    } catch (e) {

    }
}

export const dossiersSaga = function* () {
    yield takeLatest(fetchFilteredDossiers, fetchFilteredDossiersSaga);
    yield takeLatest(selectDossier, fetchDossierSaga);
    yield takeLatest(startUnloadingDossier, startUnloadingDossierSaga);
    yield takeLatest(saveAndExportDossier, saveAndExportDossierSaga);
};