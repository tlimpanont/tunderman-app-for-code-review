import {call, put, takeLatest} from "redux-saga/effects";
import api from "../api";
import {
    addDamageToArticle,
    addDamageToArticleSuccess,
} from "../actions-reducers";

function* postArticleDamageSaga({type, payload}) {
    try {
        const {data} = yield call(api.post, `/api/articles/${payload.articleUuid}/damages`, payload.damage);
        yield put(addDamageToArticleSuccess(data));
    } catch (e) {

    }
}

export const articleDamageSaga = function* () {
    yield takeLatest(addDamageToArticle, postArticleDamageSaga);
};