import {call, put, takeLatest} from "redux-saga/effects";
import api from "../api";
import {
    addPictureToDossier,
    addPictureToDossierSuccess,
    removePictureFromDossier,
    removePictureFromDossierSuccess,
} from "../actions-reducers";

function* postDossierPhotoSaga({type, payload}) {
    try {
        const {data} = yield call(api.post, `/api/dossiers/${payload.uuid}/photos`, payload.dataURL);
        yield put(addPictureToDossierSuccess(data));
    } catch (e) {

    }
}

function* deleteDossierPhotoSaga({type, payload}) {
    try {
        const {data} = yield call(api.delete, payload);
        yield put(removePictureFromDossierSuccess(data));
    } catch (e) {

    }
}

export const dossierPhotosSaga = function* () {
    yield takeLatest(addPictureToDossier, postDossierPhotoSaga);
    yield takeLatest(removePictureFromDossier, deleteDossierPhotoSaga);
};