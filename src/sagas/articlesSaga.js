import {call, put, takeLatest} from "redux-saga/effects";
import api from "../api";
import {
    addNewMarksAndNumbersToArticle, addNewMarksAndNumbersToArticleSuccess,
    finishUnloadArticle,
    finishUnloadArticleSuccess,
} from "../actions-reducers";

function* finishUnloadArticleSaga({type, payload}) {
    try {
        const requestBody = {
            uuid: payload.uuid,
            state: "FINISHED"
        };
        const {data} = yield call(api.patch, `/api/articles/${payload.uuid}`, requestBody);

        yield put(finishUnloadArticleSuccess(data));
    } catch (e) {

    }
}

function* addNewMarksAndNumbersToArticleSaga({type, payload}) {
    try {
        const {data} = yield call(api.patch, `/api/articles/${payload.uuid}`, payload);

        yield put(addNewMarksAndNumbersToArticleSuccess(data));
    } catch (e) {

    }
}

export const articlesSaga = function* () {
    yield takeLatest(finishUnloadArticle, finishUnloadArticleSaga);
    yield takeLatest(addNewMarksAndNumbersToArticle, addNewMarksAndNumbersToArticleSaga);
};