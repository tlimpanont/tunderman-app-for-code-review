import React, {PureComponent} from 'react';
import TextField from "@material-ui/core/TextField/TextField";
import {compose} from "recompose";
import {withStyles} from "@material-ui/core";

const styles = theme => ({
    bootstrapRoot: {
        padding: 20,
        'label + &': {
            marginTop: theme.spacing.unit * 3,
        },
        backgroundColor: theme.palette.primary.main
    },
    bootstrapInput: {
        borderRadius: 4,
        backgroundColor: theme.palette.common.white,
        border: '1px solid #ced4da',
        fontSize: 16,
        padding: '10px 12px',
        width: 'calc(100% - 65px)',
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        '&:focus': {
            borderColor: '#80bdff',
        },
        flexGrow: "inherit",
    },
    bootstrapFormLabel: {
        fontSize: 18,
    }
});

class _SearchBox extends PureComponent {

    constructor() {
        super();
        this.cancelSearch = this.cancelSearch.bind(this);
        this.state = {
            showCancelButton: false
        };
    }

    componentWillUpdate(nextProps, nextState, nextContext) {
    }

    cancelSearch() {
        const {onReset} = this.props;

        this.setState({showCancelButton: false});
        onReset();
    }

    render() {
        const {value, onChangeText, classes, style} = this.props;

        return (
            <TextField
                fullWidth={true}
                value={value ? value : ""}
                autoCorrect="false"
                placeholder="Dossier- of containernummer"
                onChange={(event) => onChangeText(event.target.value)}
                onFocus={() => this.setState({showCancelButton: true})}
                InputProps={{
                    disableUnderline: true,
                    classes: {
                        root: classes.bootstrapRoot,
                        input: classes.bootstrapInput,
                    },
                }}
                InputLabelProps={{
                    shrink: true,
                    className: classes.bootstrapFormLabel,
                }}
                style={style}

            >
            </TextField>
        )
    }
}

const SearchBox = compose(
    withStyles(styles, {withTheme: true})
)(_SearchBox)

export {SearchBox};