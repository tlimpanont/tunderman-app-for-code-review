import React, {PureComponent} from 'react';
import {DossierSearchItem} from './DossierSearchItem';
import List from "@material-ui/core/List/List";
import {compose} from "recompose";
import {withStyles} from "@material-ui/core";

const styles = theme => ({});

class _DossierSearchList extends PureComponent {


    constructor(props) {
        super(props);
        this.renderItem = this.renderItem.bind(this);
    }

    renderItem({item, index}) {
        const {selectedItem, onSelect} = this.props;
        const {uuid} = item;
        return <DossierSearchItem key={uuid}
                                  dossier={item}
                                  selected={uuid === selectedItem}
                                  onClick={(dossierNumber) => onSelect(dossierNumber)}/>
    }

    render() {

        const {data, classes} = this.props;
        return (
            <List disablePadding={true}>
                {data.map((item, index) => this.renderItem({item, index}))}
            </List>
        );
    }
}

const DossierSearchList = compose(
    withStyles(styles, {withTheme: true})
)(_DossierSearchList);

export {DossierSearchList};