import React from 'react';
import ListItem from "@material-ui/core/ListItem/ListItem";
import ListItemText from "@material-ui/core/ListItemText/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction/ListItemSecondaryAction";
import IconButton from "@material-ui/core/IconButton/IconButton";
import ChevronRight from "@material-ui/icons/ChevronRight";
import {compose} from 'recompose';
import withStyles from "@material-ui/core/styles/withStyles";

const styles = (theme) => {
    return {
        root: {
            backgroundColor: `${theme.palette.secondary.main} !important`,
            boxShadow: '0 4px 8px 0 rgba(0,0,0,0.12)',
            borderBottomWidth: 0
        },
        primary: {
            color: "white !important"
        },
        secondary: {
            color: "white !important"
        },
        iconButton: {
            color: "white !important"
        }
    }
};


const _DossierSearchItem = (props) => {
    const {dossier, selected, onClick, classes} = props;
    const {uuid} = dossier;
    const {dossierNumber, container} = dossier.header;

    return (
        <ListItem button onClick={() => onClick.call(this, uuid)}
                  classes={{root: (selected) ? classes.root : null}}
        >
            <ListItemText
                classes={{primary: (selected) ? classes.primary : null, secondary: (selected) ? classes.secondary : null}}
                primary={dossierNumber}
                secondary={container}
            />
            <ListItemSecondaryAction>
                <IconButton classes={{root: (selected) ? classes.iconButton : null}}>
                    <ChevronRight/>
                </IconButton>
            </ListItemSecondaryAction>
        </ListItem>
    );
};

const DossierSearchItem = compose(
    withStyles(styles, {withTheme: true})
)(_DossierSearchItem);

export {DossierSearchItem};