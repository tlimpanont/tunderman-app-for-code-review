import React from 'react';
import {compose} from "recompose";
import {withStyles, Grid, Button} from "@material-ui/core";
import {withRouter} from "react-router-dom";
import AppToolbarWithTabs from "../../common/AppToolbarWithTabs";
import {DossierHeaderCard, DossierSearchListCard} from "../../../containers/search/index";
import {connect} from "react-redux";
import {fetchFilteredDossiers} from "../../../actions-reducers/index";

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    container: {
        paddingTop: 50
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    menu: {
        width: 200,
    },
    displayCard: {
        display: 'flex',
        alignItems: "center",
        justifyContent: "center"
    }
});


class _TabsAndSearchListDetails extends React.Component {

    componentWillMount() {
        this.retrieveDossiers()
    }

    componentDidMount() {
        document.body.style.overflow = "hidden";
        this.timerID = setInterval(
            () => this.retrieveDossiers(),
            60000
        );
    }

    retrieveDossiers() {
        this.props.fetchFilteredDossiers({
            state: this.props.filterField.toUpperCase(),
            startDate: this.props.startDate,
            numberOfDays: this.props.numberOfDays
        });
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    render() {
        const {
            selectedTabIndex,
            filterField,
            selectedDossierNumber,
            classes,
            history
        } = this.props;

        return (
            <Grid container className={classes.container} id={"tabSearchList"}>
                <AppToolbarWithTabs selectedTabIndex={selectedTabIndex}/>
                <DossierSearchListCard filterField={filterField} style={{ position: "absolute", left: 15, bottom: 15, top: 130, width: "30%"}}/>
                <DossierHeaderCard style={{position: "absolute", right: 15, top: 130, width: "65%", bottom: 190, overflow: "auto"}}/>
                <Button disabled={!selectedDossierNumber}
                        variant={"raised"} color={"primary"} style={{width: 200, position: "absolute", right: 15, bottom: 140}}
                        onClick={() => {
                            history.replace(`unload/${selectedDossierNumber}`);
                        }}>
                    Lossen
                </Button>
            </Grid>
        )
    }
}

const mapStateToProps = ({selectedDossierNumber}) => {
    return {
        selectedDossierNumber: selectedDossierNumber,
    }
};

const mapDispatchToProps = (dispatch) => ({
    fetchFilteredDossiers: (filter) => {
        dispatch(fetchFilteredDossiers(filter));
    }
});

const TabsAndSearchListDetails = compose(
    connect(mapStateToProps, mapDispatchToProps),
    withStyles(styles, {withTheme: true}),
    withRouter,
)(_TabsAndSearchListDetails);

export {TabsAndSearchListDetails}