import * as React from "react";
import {AppBar, Button, Drawer, Grid, Toolbar, Typography, withStyles} from "@material-ui/core";
import {doSelectArticle} from "../../../actions-reducers/index";
import {TotalArticlesUnloadedCard} from "../../unload/index";
import {reduxForm} from 'redux-form';
import {compose} from 'recompose';
import {withRouter} from "react-router-dom";
import formValidators from "../../common/inputs/formValidators";
import {connect} from "react-redux";
import {ArticleLineForm} from "../../unload/ArticleLineForm";
import DeleteIcon from "@material-ui/icons/Delete";

const styles = (theme) => {
    return {
        paperAnchorBottom: {
            backgroundColor: theme.palette.appBackgroundHex + "!important"
        },
        view: {
            height: "100vh",
            padding: "80px 10px 0 10px",
            backgroundColor: theme.palette.appBackgroundHex
        },
        actionButtons: {
            float: "right"
        }
    }
};


class _AddLineView extends React.Component {

    constructor(props) {
        super(props);
        this.props.doSelectArticle(this.props.match.params.articleUuid);
        this.saveForm = this.saveForm.bind(this);
        this.deleteLine = this.deleteLine.bind(this);
        this.state = {
            open: false,
        };
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({open: true});
        }, 10);
        document.body.style.overflow = "scroll";
    }

    render() {
        const {classes, history, valid} = this.props;
        return (
            <Drawer anchor={"bottom"} open={this.state.open} classes={{paperAnchorBottom: classes.paperAnchorBottom}}>
                <AppBar color="primary">
                    <Toolbar>
                        <Grid container justify={"center"}>
                            <Typography variant="title" color="inherit">
                                Loslijst
                            </Typography>
                        </Grid>
                    </Toolbar>
                </AppBar>
                <div className={classes.view}>
                    <Grid container>
                        <Grid item xs={9}>

                            <ArticleLineForm articleLine={this.props.articleLine}/>

                            <div className={classes.actionButtons}>
                                <Button variant={"outlined"} color={"primary"} onClick={() => {
                                    history.goBack();
                                }}>
                                    Annuleren
                                </Button>
                                <Button variant={"outlined"} color={"primary"} onClick={this.deleteLine}>
                                    <DeleteIcon/>&nbsp; Delete
                                </Button>
                                <Button variant={"raised"} color={"primary"} onClick={this.saveForm}
                                        disabled={!valid}>
                                    Opslaan
                                </Button>
                            </div>
                        </Grid>
                        <Grid item xs={3}>
                            <TotalArticlesUnloadedCard numberOfArticlesUnloaded={this.numberOfArticlesUnloaded()}
                                                       totalNumberOfArticles={this.props.article.colli}
                                                       packing={this.props.article.packing}/>
                        </Grid>
                    </Grid>
                </div>
            </Drawer>
        )
    }

    numberOfArticlesUnloaded() {
        const articlesPerPallet = (this.props.formData.values && this.props.formData.values.articlesPerPallet)
            ? this.props.formData.values.articlesPerPallet : 0;

        const palletCount = (this.props.formData.values && this.props.formData.values.palletCount)
            ? this.props.formData.values.palletCount : 0;

        return this.props.article.lines.reduce((total, line) => {
            if (line === this.props.articleLine) {
                return total;
            }
            return total + (line.palletCount * line.articlesPerPallet)
        }, 0) + (palletCount * articlesPerPallet);
    }

    saveForm(e) {
        if (this.props.valid) {
            this.props.onSubmitForm({
                article: this.props.article,
                formData: this.props.formData.values
            });
            this.props.history.goBack();
        }
    }

    deleteLine() {
        if (this.props.articleLine && this.props.articleLine.uuid) {
            this.props.deleteLineFromArticle({
                articleUuid: this.props.article.uuid,
                lineUuid: this.props.articleLine.uuid
            });
        }
        this.props.history.goBack();
    }
}

const ArticleLineDialogView = compose(
    reduxForm({
        form: 'AddLineToArticle',
        validate: formValidators
    }),
    withRouter,
    connect((state, {match}) => {
        const article = state.selectedDossier.articles.find(article => article.uuid === match.params.articleUuid);
        return {
            article,
            formData: state.form.AddLineToArticle,
            articleLine: article.lines.find(line => line.uuid === match.params.lineUuid)
        }
    }, {doSelectArticle}),
    withStyles(styles, {withTheme: true}),
)(_AddLineView);

export { ArticleLineDialogView }