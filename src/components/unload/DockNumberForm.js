import React from 'react'
import {Field} from 'redux-form'
import {Grid, Typography, withStyles} from "@material-ui/core";
import {TextInput} from "../common/inputs/TextInput";

const styles = (theme) => {
    return {
        cardContent: {
            paddingTop: 40
        },
        label: {
            marginTop: 5,
            marginRight: 30,
            width: 200
        },
        error: {
            color: `${theme.palette.secondary.main}`,
            paddingBottom: 20
        },
        formRow: {
            flex: 1
        },
        buttonProgress: {
            color: `${theme.palette.primary.main}`,
            position: 'absolute',
            top: '50%',
            left: '50%',
            marginTop: 5,
            marginLeft: 130,
        }
    }
};

const _DockNumberForm = (props) => {
    const {classes, onSubmit} = props;
    return (
        <form>
            <Grid container className={classes.formRow}>
                <Grid item xs={4}>
                    <Typography variant={"body1"} component={"label"} className={classes.label}>Dock nummer *</Typography>
                </Grid>
                <Grid item xs={8}>
                    <Field name="dockNumber" component={TextInput} onEnterPress={() => onSubmit()}/>
                </Grid>
            </Grid>
        </form>
    )
};

const DockNumberForm = withStyles(styles, {withTheme: true})(_DockNumberForm);

export {DockNumberForm}