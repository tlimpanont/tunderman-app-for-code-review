import React from 'react'
import {Field} from 'redux-form'
import {Grid, Typography, withStyles} from "@material-ui/core";
import {TextInput} from "../common/inputs/TextInput";

const styles = (theme) => {
    return {
        cardContent: {
            paddingTop: 40
        },
        label: {
            marginTop: 5,
            marginRight: 30,
            width: 200
        },
        error: {
            color: `${theme.palette.secondary.main}`,
            paddingBottom: 20
        },
        formRow: {
            flex: 1
        },
        buttonProgress: {
            color: `${theme.palette.primary.main}`,
            position: 'absolute',
            top: '50%',
            left: '50%',
            marginTop: 5,
            marginLeft: 130,
        }
    }
};

const _LocationForm = ({classes, onSubmit}) => {
    return (
        <form>
            <Grid container className={classes.formRow}>
                <Grid container className={classes.formRow}>
                    <Grid item xs={3}>
                        <Typography variant={"body1"} component={"label"}
                                    className={classes.label}>Locatie *</Typography>
                    </Grid>
                    <Grid item xs={9}>
                        <Field name={"location"} component={TextInput} onEnterPress={() => onSubmit()}/>
                    </Grid>
                </Grid>
            </Grid>
        </form>
    )
};

const LocationForm = withStyles(styles, {withTheme: true})(_LocationForm);

export {LocationForm}