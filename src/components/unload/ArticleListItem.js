import React from 'react';
import {ListItem, ListItemIcon, ListItemSecondaryAction, ListItemText} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton/IconButton";
import ChevronRight from "@material-ui/icons/ChevronRight";
import {compose} from 'recompose';
import withStyles from "@material-ui/core/styles/withStyles";
import CheckIcon from '@material-ui/icons/Check';
import classnames from "classnames";

const styles = (theme) => {
    return {
        root: {
            backgroundColor: `${theme.palette.secondary.main} !important`,
            boxShadow: '0 4px 8px 0 rgba(0,0,0,0.12)',
            borderBottomWidth: 0
        },
        selected: {
            color: "white !important"
        },
        finished: {
            opacity: 0.5
        },
        hidden: {
            visibility: "hidden"
        }
    }
};


function getFirstLine(marksAndNumbers) {
    if (marksAndNumbers) {
        return marksAndNumbers.split('\n').slice(0, 1).map((item) => { return item });
    }
    return "";
}

Number.prototype.pad = function(size) {
    var s = String(this);
    while (s.length < (size || 2)) {s = "0" + s;}
    return s;
};


const _ArticleListItem = (props) => {
    const {article, selected, onClick, classes} = props;
    const {colli, uuid, packing, marksAndNumbers, state, itemNumber, specNumber} = article;
    const finished = state === "FINISHED";

    return (
        <ListItem button onClick={() => onClick.call(this, uuid)}
                  classes={{root: (selected) ? classes.root : null}}
        >
            <ListItemIcon style={{visibility: !finished && "hidden"}}
                          classes={{
                              root: classnames({
                                  [classes.selected]: selected,
                                  [classes.hidden]: !finished,
                              })
                          }}>
                <CheckIcon/>
            </ListItemIcon>
            <ListItemText
                classes={{
                    primary: classnames({
                        [classes.selected]: selected,
                        [classes.finished]: finished && !selected,
                    })
                }}
                primary={ itemNumber.pad(2) + "-" + specNumber.pad(2) + " " + colli + " " + packing + " " + getFirstLine(marksAndNumbers)}
            />
            <ListItemSecondaryAction>
                <IconButton classes={{
                    root: classnames({
                        [classes.selected]: selected,
                        [classes.finished]: finished && !selected,
                    })
                }}>
                    <ChevronRight/>
                </IconButton>
            </ListItemSecondaryAction>
        </ListItem>
    );
};

const ArticleListItem = compose(
    withStyles(styles, {withTheme: true})
)(_ArticleListItem);

export {ArticleListItem};