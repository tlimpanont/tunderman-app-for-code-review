import React from 'react';
import {List, withStyles} from "@material-ui/core/";
import {compose} from "recompose";
import {ArticleListItem} from "./ArticleListItem";
import {connect} from "react-redux";

const styles = theme => ({});

function renderItem(item, selectedArticleId, onSelect) {
    const {uuid} = item;

    return (
        <ArticleListItem
            key={uuid}
            article={item}
            selected={uuid === selectedArticleId}
            onClick={(uuid) => onSelect(uuid)}
        />
    );
}

class _ArticleList extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {data, selectedArticleId, onSelect, style} = this.props;

        return (
            <List disablePadding={true} style={style}>
                {data.map((item) => renderItem(item, selectedArticleId, onSelect))}
            </List>
        );
    }
}

export const ArticleList = compose(
    connect(({expandDossierHeader}) => {
        return {
            expandDossierHeader
        }
    }),
    withStyles(styles, {withTheme: true})
)(_ArticleList);
