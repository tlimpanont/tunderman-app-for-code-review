import React from "react";
import {Table, TableBody, TableCell, TableHead, TableRow, withStyles} from "@material-ui/core";

const styles = (theme) => {
    return {
        root: {
            tableLayout: "fixed",
        },
        tableCell: {
            padding: "0 4px",
            color: `${theme.palette.primary.main}`,
            border: 'none',
            whiteSpace: "nowrap",
            overflow: "hidden",
            textOverflow: "ellipsis",
        },
        tableHead: {
            borderBottom: `3px solid ${theme.palette.primary.main} `,
            textAlign: "left",
        },
        tableRow: {
            whiteSpace: "nowrap",
            overflow: "hidden",
            textOverflow: "ellipsis",
            '&:nth-of-type(odd)': {
                backgroundColor: theme.palette.background.default,
            },
        },
    }
};

export const ArticleLinesTable = withStyles(styles, {withTheme: true})((props) => {
    const {rows, classes, onSelectRowItem} = props;

    return (
        <Table className={classes.root}>
            <TableHead className={classes.tableHead}>
                <TableRow className={classes.tableRow}>
                    <TableCell className={classes.tableCell}>Pallet Type</TableCell>
                    <TableCell className={classes.tableCell} numeric># Pallets</TableCell>
                    <TableCell className={classes.tableCell} numeric>Aantal</TableCell>
                    <TableCell className={classes.tableCell} numeric>L</TableCell>
                    <TableCell className={classes.tableCell} numeric>B</TableCell>
                    <TableCell className={classes.tableCell} numeric>H</TableCell>
                    <TableCell className={classes.tableCell}>Stackable</TableCell>
                    <TableCell className={classes.tableCell}>Locatie</TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {rows.map(row => {
                    return (
                        <TableRow hover key={Math.random() * 1000000} className={classes.tableRow} onClick={(event) => {
                            onSelectRowItem.apply(this, [event, row]);
                        }}>
                            <TableCell component="th" scope="row" className={classes.tableCell}>{row.palletType}</TableCell>
                            <TableCell className={classes.tableCell} numeric>{row.palletCount}</TableCell>
                            <TableCell className={classes.tableCell} numeric>{row.articlesPerPallet}</TableCell>
                            <TableCell className={classes.tableCell} numeric>{row.length}</TableCell>
                            <TableCell className={classes.tableCell} numeric>{row.width}</TableCell>
                            <TableCell className={classes.tableCell} numeric>{row.height}</TableCell>
                            <TableCell className={classes.tableCell}>{row.stackable ? "Y" : "N"}</TableCell>
                            <TableCell className={classes.tableCell}>{row.location}</TableCell>
                        </TableRow>
                    );
                })}
            </TableBody>
        </Table>
    )
});