import React from 'react'
import {Field} from 'redux-form'
import {Grid, Typography, withStyles} from "@material-ui/core";
import {TextInput} from "../common/inputs/TextInput";
import {SchadeInput} from "../common/inputs/SchadeInput";
import {SchadeCodeInput} from "../common/inputs/SchadeCodeInput";

const styles = (theme) => {
    return {
        cardContent: {
            paddingTop: 40
        },
        label: {
            marginTop: 5,
            marginRight: 30,
            width: 200
        },
        error: {
            color: `${theme.palette.secondary.main}`,
            paddingBottom: 20
        },
        formRow: {
            flex: 1
        },
        buttonProgress: {
            color: `${theme.palette.primary.main}`,
            position: 'absolute',
            top: '50%',
            left: '50%',
            marginTop: 5,
            marginLeft: 130,
        }
    }
};

const _DamageForm = ({classes, damageType, damageCode}) => {
    return (
        <form>
            <Grid container className={classes.formRow}>
                <Grid item xs={3}>
                    <Typography variant={"body1"} component={"label"}
                                className={classes.label}>Schade *</Typography>
                </Grid>
                <Grid item xs={9}>
                    <Field name={"damageType"} component={SchadeInput}/>
                </Grid>
            </Grid>

            {damageType === 'DAMAGE' && (
                <Grid container className={classes.formRow}>
                    <Grid item xs={3}>
                        <Typography variant={"body1"} component={"label"}
                                    className={classes.label}>Code *</Typography>
                    </Grid>
                    <Grid item xs={9}>
                        <Field name={"damageCode"} component={SchadeCodeInput}/>
                    </Grid>
                </Grid>)
            }
            {damageType === 'DAMAGE' && (
                <Grid container className={classes.formRow}>
                    <Grid item xs={3}>
                        <Typography variant={"body1"} component={"label"}
                                    className={classes.label}>Bemerking *</Typography>
                    </Grid>
                    <Grid item xs={9}>
                        <Field name={"remark"} component={TextInput}/>
                    </Grid>
                </Grid>)
            }

        </form>
    )
};

const DamageForm = withStyles(styles, {withTheme: true})(_DamageForm);

export {DamageForm}