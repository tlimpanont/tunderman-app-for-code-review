import {Card, CardContent, Grid, Typography, withStyles} from "@material-ui/core";
import {Field, reduxForm} from "redux-form";
import {NumberInput} from "../common/inputs/NumberInput";
import {PalletTypesInput} from "../common/inputs/PalletTypesInput";
import {MetricsInput} from "../common/inputs/MetricsInput";
import {TextInput} from "../common/inputs/TextInput";
import {SwitchInput} from "../common/inputs/SwitchInput";
import * as React from "react";
import {connect} from "react-redux";
import {asyncFormValidators} from "../common/inputs/asyncFormValidators";


const styles = (theme) => {
    return {
        card: {
            height: "95%"
        },
        cardContent: {
            paddingTop: 40
        },
        label: {
            marginTop: 5,
        },
        formRow: {
            margin: "4px 0"
        },
    }
};

const _ArticleLineForm = ((props) => {
    const {classes, dispatch} = props;
    return (
        <Card className={classes.card}>
            <CardContent className={classes.cardContent}>
                <Grid container className={classes.formRow}>
                    <Grid item xs={3}>
                        <Typography variant={"body1"} component={"label"} className={classes.label}>Aantal
                            pallets *</Typography>
                    </Grid>
                    <Grid item xs={9}>
                        <Field name="palletCount" component={NumberInput}
                               parse={value => Number(value)}/>
                    </Grid>

                    <Grid item xs={3}>
                        <Typography variant={"body1"} component={"label"}
                                    className={classes.label}>Artikelen/pallet *</Typography>
                    </Grid>
                    <Grid item xs={9}>
                        <Field name="articlesPerPallet" component={NumberInput}
                               parse={value => Number(value)}/>
                    </Grid>
                </Grid>
                <Grid container className={classes.formRow}>
                    <Grid item xs={3}>
                        <Typography variant={"body1"} component={"label"} className={classes.label}>Pallet
                            type *</Typography>
                    </Grid>
                    <Grid item xs={9}>
                        <Field name="palletType" component={PalletTypesInput}
                        />
                    </Grid>
                </Grid>

                <Grid container className={classes.formRow}>
                    <Grid item xs={3}>
                        <Typography variant={"body1"} component={"label"}
                                    className={classes.label}>Lengte *</Typography>
                    </Grid>
                    <Grid item xs={9}>
                        <Field name={"length"} component={MetricsInput}/>
                    </Grid>
                </Grid>

                <Grid container className={classes.formRow}>
                    <Grid item xs={3}>
                        <Typography variant={"body1"} component={"label"}
                                    className={classes.label}>Breedte *</Typography>
                    </Grid>
                    <Grid item xs={9}>
                        <Field name={"width"} component={MetricsInput}/>
                    </Grid>
                </Grid>

                <Grid container className={classes.formRow}>
                    <Grid item xs={3}>
                        <Typography variant={"body1"} component={"label"}
                                    className={classes.label}>Hoogte *</Typography>
                    </Grid>
                    <Grid item xs={9}>
                        <Field name={"height"} component={MetricsInput}/>
                    </Grid>
                </Grid>

                <Grid container className={classes.formRow}>
                    <Grid item xs={3}>
                        <Typography variant={"body1"} component={"label"}
                                    className={classes.label}>Locatie</Typography>
                    </Grid>
                    <Grid item xs={9}>
                        <Field name={"location"} component={TextInput}/>
                    </Grid>
                </Grid>

                <Grid container className={classes.formRow}>
                    <Grid item xs={3}>
                        <Typography variant={"body1"} component={"label"} className={classes.label}>
                            Stackable
                        </Typography>
                    </Grid>
                    <Grid item xs={9}>
                        <Field name={"stackable"} component={SwitchInput}/>
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    )
});

// Decorate with reduxForm(). It will read the initialValues prop provided by connect()
const AddLineToArticleForm = reduxForm({
    form: 'AddLineToArticle', // a unique identifier for this form
    asyncValidate: asyncFormValidators,
    asyncBlurFields: ['location']
})(withStyles(styles, {withTheme: true})(_ArticleLineForm));

// You have to connect() to any reducers that you wish to connect to yourself
export const ArticleLineForm = connect(
    (state, {articleLine}) => {
        return {
            initialValues: articleLine // pull initial values from account reducer
        }
    }
)(AddLineToArticleForm);