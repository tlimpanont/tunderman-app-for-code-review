import { AppBar, IconButton, Toolbar, Typography } from "@material-ui/core";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ImportExportIcon from "@material-ui/icons/ImportExport";
import React from "react";
import { withRouter } from "react-router-dom";
import { Grid}  from "@material-ui/core";

const _AppToolbar = ({history, onBack, onSaveAndExport}) => {

        return (
            <AppBar position="fixed" color="primary">
                <Toolbar>
                    <IconButton color="inherit" aria-label="Back" style={{
                        width: 90
                    }} onClick={() => onBack()}>
                        <ChevronLeftIcon/>
                        <Typography variant="body1" color="inherit">
                            Terug
                        </Typography>
                    </IconButton>
                    <Grid container justify={"center"}>
                        <Typography variant="title" color="inherit">
                            Loslijst
                        </Typography>
                    </Grid>
                    <IconButton color="inherit" aria-label="Import & Export" onClick={() => onSaveAndExport()}
                                style={{width: 260}}>
                        <ImportExportIcon/>
                        <Typography variant="body1" color="inherit">
                            Opslaan en Exporteren
                        </Typography>
                    </IconButton>
                </Toolbar>
            </AppBar>
        )
};

const AppToolbar = withRouter(_AppToolbar);

export { AppToolbar };