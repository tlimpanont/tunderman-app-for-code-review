import React from 'react'
import {Field} from 'redux-form'
import {Grid, Typography, withStyles} from "@material-ui/core";
import {TextInput} from "../common/inputs/TextInput";
import {SchadeInput} from "../common/inputs/SchadeInput";
import {SchadeCodeInput} from "../common/inputs/SchadeCodeInput";

const styles = (theme) => {
    return {
        cardContent: {
            paddingTop: 40
        },
        label: {
            marginTop: 5,
            marginRight: 30,
            width: 200
        },
        error: {
            color: `${theme.palette.secondary.main}`,
            paddingBottom: 20
        },
        formRow: {
            flex: 1
        },
        buttonProgress: {
            color: `${theme.palette.primary.main}`,
            position: 'absolute',
            top: '50%',
            left: '50%',
            marginTop: 5,
            marginLeft: 130,
        }
    }
};

const _MarksAndNumbersForm = ({classes}) => {
    return (
        <form>
            <Grid container className={classes.formRow}>
                <Grid item xs={3}>
                    <Typography variant={"body1"} component={"label"}
                                className={classes.label}>Merken</Typography>
                </Grid>
                <Grid item xs={9}>
                    <Field name={"newMarksAndNumbers"} component={TextInput} multiline rows="3"
                           rowsMax={3}/>
                </Grid>
            </Grid>
        </form>
    )
};

const MarksAndNumbersForm = withStyles(styles, {withTheme: true})(_MarksAndNumbersForm);

export {MarksAndNumbersForm}