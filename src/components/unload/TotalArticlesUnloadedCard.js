import React from 'react';
import {Card, CardHeader, CardContent, Typography} from "@material-ui/core";
import { withStyles } from '@material-ui/core/styles';
import classnames from "classnames";
import {compose} from "recompose";

const styles = (theme) => {
    return {
        card: {
            height: 'auto'
        },
        counter: {
            fontSize: 48,

            padding: '10px',
            margin: '20px'
        },
        border: {
            borderWidth: 2,
            borderColor: `${theme.palette.secondary.main}`,
            borderStyle: 'solid'
        },
        correct: {
            borderColor: `#54C242`,
        }
    }
};

const _TotalArticlesUnloadedCard = (props) => {
    const {numberOfArticlesUnloaded, totalNumberOfArticles, packing, classes} = props;
    const quote =  numberOfArticlesUnloaded + " / " + totalNumberOfArticles;
    return (
        <Card className={classes.card}>
            <CardHeader title="Totaal gelost" className={"__has-border"}/>
            <CardContent>
                <Typography align={'center'} className={
                    classnames(classes.counter, classes.border, {
                    [classes.correct]: numberOfArticlesUnloaded === totalNumberOfArticles
                    })}
                            color={"primary"}>
                    {quote} {packing}
                </Typography>
                <Typography color={"primary"} style={{fontSize: '24px', fontWeight: 'bolder'}}>Nog te lossen</Typography>
                <Typography align={'center'} color={"primary"} className={classnames(classes.counter)} >{totalNumberOfArticles - numberOfArticlesUnloaded}</Typography>
            </CardContent>
        </Card>
    );
};

const TotalArticlesUnloadedCard = compose(
    withStyles(styles, {withTheme: true})
)(_TotalArticlesUnloadedCard);

export { TotalArticlesUnloadedCard };