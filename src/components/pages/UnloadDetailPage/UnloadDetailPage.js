import React, {Component} from 'react';
import {connect} from 'react-redux';
import {compose} from 'recompose';
import {Route, withRouter} from "react-router-dom";
import {
    addLineToArticle,
    doResetArticleSelection, editArticleLine,
    findByDossierNumber,
    resetDossierNumberSelection,
    saveAndExportDossier,
    selectDossierNumber,
    deleteLineFromArticle, startUnloadingDossier
} from "../../../actions-reducers/index";
import {Grid} from "@material-ui/core";
import {ArticleDetailCard, ArticleHeaderCard, ArticlesCard, DossierHeaderCard} from "../../../containers/unload/index";
import {AppToolbar} from "../../unload/index";
import {DockNumberDialog, OkCancelDialog} from "../../common/index";
import {ArticleLineDialogView} from "../../organisms";
import {selectDossier} from "../../../actions-reducers/selectionDossier";
import {withStyles} from "@material-ui/core/styles/index";
import {PhotoDialog} from "../../common/PhotoDialog";

const styles = {
    container: {
        paddingTop: 50
    },
    dossierHeaderStyle: {
        position: "absolute",
        left: 15,
        right: 15,
        top: 80,
    },
    dossierHeaderContentStyle: {
        top: 65,
        height: "195px",
        overflow: "scroll"
    },
    articlesSmallStyle: {
        position: "absolute",
        left: 15,
        bottom: 15,
        top: 400,
        width: "30%"
    },
    articlesLargeStyle: {
        position: "absolute",
        left: 15,
        bottom: 15,
        top: 400 - 235,
        width: "30%"
    },
    articleHeaderSmallStyle: {
        position: "absolute",
        width: "65%",
        right: 15,
        top: 400
    },
    articleHeaderLargeStyle: {
        position: "absolute",
        width: "65%",
        right: 15,
        top: 400 - 235
    },
    articleHeaderContentStyle: {
        top: 65,
        height: "190px",
        overflow: "scroll"
    },
    articleDetailHeaderExpandedStyle: {
        position: "absolute",
        right: 15,
        bottom: 15,
        top: 90 + 300 + 15 + 65 + 15,
        width: "65%"
    },
    articleDetailArticleHeaderExpandedStyle: {
        position: "absolute",
        right: 15,
        bottom: 15,
        top: 90 + 65 + 15 + 65 + 230 + 15,
        width: "65%"
    },
    articleDetailAllCollapsedStyle: {
        position: "absolute",
        right: 15,
        bottom: 15,
        top: 90 + 65 + 15 + 65 + 15,
        width: "65%"
    },
    articleDetailAllExpandedStyle: {
        position: "absolute",
        right: 15,
        bottom: 15,
        top: 90 + 300 + 15 + 65 + 230 + 15,
        width: "65%"
    }
};

class _UnloadDetailPage extends Component {

    constructor(props) {
        super(props);
        this.onBack = this.onBack.bind(this);
        this.prepareSaveAndExport = this.prepareSaveAndExport.bind(this);
        this.handleCancelPartialExport = this.handleCancelPartialExport.bind(this);
        this.handlePartialExport = this.handlePartialExport.bind(this);
        this.handleEndPictureAlreadyTaken = this.handleEndPictureAlreadyTaken.bind(this);
        this.handleTakeEndPicture = this.handleTakeEndPicture.bind(this);
        this.showDialogDockNumber = this.showDialogDockNumber.bind(this);
        this.hideDialogDockNumber = this.hideDialogDockNumber.bind(this);
        this.startUnloadingDossier = this.startUnloadingDossier.bind(this);
        this.props.selectDossierNumber(this.props.match.params.dossierNumber);
        this.props.selectDossier(this.props.match.params.dossierNumber);
        this.state = {
            showDialogPartialExport: false,
            showDialogEndPicture: false,
            showDialogDockNumber: false,
            showPicture: false,
            pictureUrl: null
        };
    }

    showDialogPartialExport() {
        this.setState({showDialogPartialExport: true});
    }

    hideDialogPartialExport() {
        this.setState({showDialogPartialExport: false});
    }

    showDialogEndPicture() {
        this.setState({showDialogEndPicture: true});
    }

    hideDialogEndPicture() {
        this.setState({showDialogEndPicture: false});
    }

    showDialogDockNumber() {
        this.setState({showDialogDockNumber: true});
    }

    hideDialogDockNumber() {
        this.setState({showDialogDockNumber: false});
    }

    startUnloadingDossier(data) {
        this.hideDialogDockNumber();
        this.props.startUnloadingDossier(data);
    }

    handlePartialExport() {
        this.hideDialogPartialExport();
        this.showDialogEndPicture();
    }

    handleCancelPartialExport() {
        this.hideDialogPartialExport();
    }

    handleEndPictureAlreadyTaken() {
        this.hideDialogEndPicture();
        this.saveAndExport();
    }

    handleTakeEndPicture() {
        this.hideDialogEndPicture();
    }

    prepareSaveAndExport() {
        const {selectedDossier} = this.props;
        if (!selectedDossier) {
            return
        }
        if (!selectedDossier.articles || selectedDossier.articles.some(e => e.state !== 'FINISHED')) {
            this.showDialogPartialExport();
        } else {
            this.showDialogEndPicture();
        }
    }

    showPicture(picture) {
        this.setState({
            showPicture: true,
            pictureUrl: picture
        });
    }

    hidePicture() {
        this.setState({
            showPicture: false,
            pictureUrl: null
        });
    }

    saveAndExport() {
        const {selectedDossier, saveAndExportDossier, history} = this.props;
        saveAndExportDossier({uuid: selectedDossier.uuid});
        history.replace("/");
    }

    onBack() {
        this.props.onBack();
        this.props.history.replace("/");
    }

    componentDidMount() {
        document.body.style.overflow = "scroll";
    }

    render() {
        const { classes, expandDossierHeader, expandArticleHeader, selectedDossier, onBack } = this.props;

        return (
            <Grid container className={classes.container}>
                <AppToolbar onBack={this.onBack} onSaveAndExport={this.prepareSaveAndExport}/>
                <OkCancelDialog
                    title={'Container nog niet volledig gelost'}
                    content={'Nog niet alle artikelen zijn gelost. ' +
                    'Wilt u deze container toch (gedeeltelijk) exporteren?'}
                    open={this.state.showDialogPartialExport}
                    onCancel={this.handleCancelPartialExport}
                    onOk={this.handlePartialExport}/>
                <OkCancelDialog
                    title={'Eindfoto al gemaakt?'}
                    content={'Is er al een eindfoto gemaakt van de lege container?'}
                    open={this.state.showDialogEndPicture}
                    onCancel={this.handleEndPictureAlreadyTaken}
                    onOk={this.handleTakeEndPicture}
                    okText={'Nu foto maken'}
                    cancelText={'Al gemaakt'}/>
                <DockNumberDialog
                    open={this.state.showDialogDockNumber}
                    onCancel={this.hideDialogDockNumber}
                    onOk={this.startUnloadingDossier}/>
                <PhotoDialog
                    open={this.state.showPicture}
                    onClose={() => this.hidePicture()}
                    url={this.state.pictureUrl}
                />
                <DossierHeaderCard
                    startUnloadingDossier={this.showDialogDockNumber}
                    selectPicture={(picture) => this.showPicture(picture)}
                    styles={{
                        rootStyle: styles.dossierHeaderStyle,
                        contentStyle: styles.dossierHeaderContentStyle
                    }}/>
                <ArticlesCard style={expandDossierHeader ? styles.articlesSmallStyle:  styles.articlesLargeStyle}/>
                <ArticleHeaderCard styles={{
                    rootStyle: expandDossierHeader ? styles.articleHeaderSmallStyle:  styles.articleHeaderLargeStyle,
                    contentStyle: styles.articleHeaderContentStyle
                }}/>
                <ArticleDetailCard style={
                    expandDossierHeader ?
                        (expandArticleHeader ? styles.articleDetailAllExpandedStyle : styles.articleDetailHeaderExpandedStyle) :
                        (expandArticleHeader ? styles.articleDetailArticleHeaderExpandedStyle: styles.articleDetailAllCollapsedStyle)
                }
                                   onSelectPicture={(picture) => this.showPicture(picture)}
                />
                <Route path={`${this.props.match.url}/article/:articleUuid/line/add`} render={() => {
                    return (
                        (selectedDossier) ?
                            <ArticleLineDialogView
                                onSubmitForm={({formData, article}) => {
                                    this.props.addLineToArticle({
                                        article,
                                        newLine: formData
                                    })
                                }}
                                deleteLineFromArticle={(data) => this.props.deleteLineFromArticle(data)
                                }
                            />
                            : null
                    )
                }}/>

                <Route path={`${this.props.match.url}/article/:articleUuid/line/:lineUuid/edit`} render={() => {
                    return (
                        (selectedDossier) ?
                            <ArticleLineDialogView
                                onSubmitForm={({formData, article}) => {
                                    this.props.editArticleLine({
                                        article,
                                        newLine: formData
                                    })
                                }}
                                
                                deleteLineFromArticle={(data) => this.props.deleteLineFromArticle(data)
                                }
                            />
                            : null
                    )
                }}/>

            </Grid>
        );
    }
}

const mapStateToProps = ({dossiers, selectedDossier, expandDossierHeader, expandArticleHeader}, {match}) => {
    return {
        selectedDossier: selectedDossier,
        expandDossierHeader: expandDossierHeader,
        expandArticleHeader: expandArticleHeader
    }
};

const mapDispatchToProps = (dispatch) => ({
    selectDossierNumber: (dossierNumber) => {
        dispatch(selectDossierNumber(dossierNumber));
    },
    selectDossier: (dossierNumber) => {
        dispatch(selectDossier(dossierNumber));
    },
    saveAndExportDossier: (data) => {
        dispatch(saveAndExportDossier(data));
        dispatch(resetDossierNumberSelection());
        dispatch(doResetArticleSelection());
    },
    addLineToArticle: (data) => {
        dispatch(addLineToArticle(data));
    },
    editArticleLine:(data) => {
        dispatch(editArticleLine(data));
    },
    deleteLineFromArticle: (data) => {
        dispatch(deleteLineFromArticle(data));
    },
    startUnloadingDossier: (data) => {
        dispatch(startUnloadingDossier(data));
    },
    onBack: () => {
        dispatch(resetDossierNumberSelection());
        dispatch(doResetArticleSelection());
    },
});

const UnloadDetailPage = compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps),
    withStyles(styles)
)(_UnloadDetailPage);

export { UnloadDetailPage };
