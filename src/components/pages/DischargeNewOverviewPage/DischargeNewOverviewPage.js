import React from 'react';
import { TabsAndSearchListDetails } from "../../organisms";

const DischargeNewOverviewPage = () => {
        return (
            <TabsAndSearchListDetails selectedTabIndex={0} filterField={'new'} startDate={new Date().toISOString().slice(0, 10)} numberOfDays={1}/>
        );
};

export { DischargeNewOverviewPage };

