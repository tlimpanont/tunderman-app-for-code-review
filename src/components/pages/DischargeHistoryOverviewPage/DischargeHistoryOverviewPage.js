import React from 'react';
import { TabsAndSearchListDetails } from "../../organisms";

const DischargeHistoryOverviewPage = () =>  {
    return (
        <TabsAndSearchListDetails selectedTabIndex={2} filterField={'done'}/>
    );
};

export { DischargeHistoryOverviewPage };

