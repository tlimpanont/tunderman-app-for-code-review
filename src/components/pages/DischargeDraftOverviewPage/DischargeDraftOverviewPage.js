import React from 'react';
import {TabsAndSearchListDetails} from "../../organisms";

const DischargeDraftOverviewPage = () => {
    return (
        <TabsAndSearchListDetails selectedTabIndex={1} filterField={'draft'}/>
    );
};

export { DischargeDraftOverviewPage };

