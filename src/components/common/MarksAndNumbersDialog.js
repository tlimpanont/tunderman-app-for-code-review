import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import {Grid, Zoom} from "@material-ui/core";
import {formValueSelector, reduxForm} from "redux-form";
import {connect} from "react-redux";
import {compose} from "recompose";
import {findByArticleId} from "../../actions-reducers";
import {MarksAndNumbersForm} from "../unload/MarksAndNumbersForm";

function Transition(props) {
    return <Zoom in="true" {...props} />;
}

const validator = () => {
    return {};
};

const _MarksAndNumbersDialog = (props) => {
    const {open, onCancel, onOk, valid, initialValues, formData, article} = props;
    
    return (
            <Dialog
                open={open}
                TransitionComponent={Transition}
                keepMounted
                onClose={() => onCancel()}
            >
                <DialogTitle>
                    Geef merken op
                </DialogTitle>
                <DialogContent>
                    <MarksAndNumbersForm initialValues={initialValues}/>
                </DialogContent>
                <DialogActions>
                    <Grid container justify={"flex-end"} style={{marginTop: 30}}>
                        <Button onClick={() => onCancel()} variant={"outlined"} color="default">
                            Cancel
                        </Button>
                        <Button disabled={!valid} onClick={() => onOk(
                                    {
                                        uuid: article.uuid,
                                        newMarksAndNumbers: formData.values.newMarksAndNumbers
                                    }
                                )}
                                variant={"raised"}
                                color={"primary"}>
                            Ok
                        </Button>
                    </Grid>
                </DialogActions>
            </Dialog>
    );
};

const mapStateToProps = (state) => {
    const selectedArticle = findByArticleId(state.selectedDossier, state.selectedArticleId);
    return {
        formData: state.form.marksAndNumbers,
        initialValues: selectedArticle ? { newMarksAndNumbers: selectedArticle.newMarksAndNumbers } : null,
        article: selectedArticle
    }
};

const mapDispatchToProps = () => ({
});

const MarksAndNumbersDialog = compose(
    connect(mapStateToProps, mapDispatchToProps),
    reduxForm({
        form: 'marksAndNumbers',
        enableReinitialize: true,
        validate: validator
    })
)(_MarksAndNumbersDialog);

export { MarksAndNumbersDialog };
