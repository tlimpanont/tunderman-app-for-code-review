import React, {PureComponent} from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import {DockNumberForm} from "../unload";
import {Grid} from "@material-ui/core";
import {reduxForm} from "redux-form";
import {GetPictureButton} from "./GetPictureButton";
import {connect} from "react-redux";
import {compose} from "recompose";
import {addPictureToDossier} from "../../actions-reducers";

function Transition(props) {
    return <Slide direction="down" {...props} />;
}

const validator = (values) => {
    const errors = {};
    const requiredFields = [
        'dockNumber'
    ];

    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = {message: 'Veld is verplicht'};
        }
    });

    return errors;
};

class _DockNumberDialog extends PureComponent {

    render() {
        const {open, onCancel, onOk, valid, formData, dossier, addPictureToDossier} = this.props;

        return (
            <Dialog
                open={open}
                TransitionComponent={Transition}
                keepMounted
                onClose={() => onCancel()}
            >
                <DialogTitle>
                    Waar wordt de container gelost?
                </DialogTitle>
                <DialogContent>
                    <DockNumberForm onSubmit={() => this.okButton.click()}/>
                </DialogContent>
                <DialogActions>
                    <Grid container justify={"flex-end"} style={{marginTop: 30}}>
                        <Button onClick={() => onCancel()} variant={"outlined"} color="default">
                            Cancel
                        </Button>
                        <GetPictureButton
                            ref={okButton => this.okButton = okButton}
                            disabled={!valid}
                            buttonIcon={null}
                            buttonText={"Ok"}
                            variant={"raised"}
                            color={"primary"}
                            onClick={() => {
                                onOk({
                                    uuid: dossier.uuid,
                                    dockNumber: formData.values.dockNumber,
                                })
                            }}
                            onGetPictureSuccess={(dataURL) => {
                                addPictureToDossier({
                                    uuid: dossier.uuid,
                                    dataURL: dataURL
                                })
                            }}/>
                    </Grid>
                </DialogActions>
            </Dialog>
        );
    }
}


const mapStateToProps = ({form, selectedDossier}) => {
    return {
        formData: form.docknumber,
        dossier: selectedDossier
    }
};

const mapDispatchToProps = (dispatch) => ({
    addPictureToDossier: (data) => {
        dispatch(addPictureToDossier(data));
    }
});

const DockNumberDialog = compose(
    reduxForm({
        form: 'docknumber',
        validate: validator
    }),
    connect(mapStateToProps, mapDispatchToProps)
)(_DockNumberDialog);

export { DockNumberDialog };
