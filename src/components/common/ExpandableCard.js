import React from 'react';
import {CardContent, Collapse, IconButton} from "@material-ui/core/";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {withStyles} from '@material-ui/core/styles';
import classnames from 'classnames';
import {Card, CardHeader} from "@material-ui/core";

const styles = theme => ({
    expand: {
        transform: 'rotate(0deg)',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
        marginLeft: 'auto',
        [theme.breakpoints.up('sm')]: {
            marginRight: 0,
            marginBottom: -10
        },
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    }
});

const _ExpandableCard = ({ children, title = '', subheader = null, expanded = true, onCollapse, onExpand, classes, styles }) => {

    return (
        <Card style={styles ? styles.rootStyle : styles}>
            <CardHeader
                style={{cursor: "pointer"}}
                onClick={() => {
                    if (expanded) {
                        onCollapse()
                    } else {
                        onExpand()
                    }
                }}
                action={
                    <IconButton
                        className={classnames(classes.expand, {
                            [classes.expandOpen]: expanded,
                        })}
                        color={"primary"}
                        aria-expanded={expanded}
                        aria-label="Show more"
                    >
                        <ExpandMoreIcon/>
                    </IconButton>
                }
                title={ title }
                subheader={ subheader }
                className={(expanded) ? "__has-border" : null}
            />
            <Collapse in={expanded} timeout="auto" unmountOnExit>
                <CardContent style={styles ? styles.contentStyle : styles}>
                    { children }
                </CardContent>
            </Collapse>
        </Card>
    );
};

const ExpandableCard = withStyles(styles)(_ExpandableCard);
export { ExpandableCard };