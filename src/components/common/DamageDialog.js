import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import {Grid} from "@material-ui/core";
import {formValueSelector, reduxForm} from "redux-form";
import {connect} from "react-redux";
import {compose} from "recompose";
import {findByArticleId} from "../../actions-reducers";
import {DamageForm} from "../unload/DamageForm";

function Transition(props) {
    return <Slide direction="down" {...props} />;
}

const validator = (values) => {
    const errors = {};
    const requiredFields = [
        'damageType'
    ];

    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = {message: 'Veld is verplicht'};
        }
    });

    if (values.damageType === 'DAMAGE' && !values.remark) {
        errors.remark = {message: 'Veld is verplicht'};
    }
    if (values.damageType === 'DAMAGE' && !values.damageCode) {
        errors.damageCode = {message: 'Veld is verplicht'};
    }
    
    return errors;
};

const _DamageDialog = (props) => {
    const {open, onCancel, onOk, damageType, valid, initialValues, formData, article} = props;
    
    return (
            <Dialog
                open={open}
                TransitionComponent={Transition}
                keepMounted
                onClose={() => onCancel()}
            >
                <DialogTitle>
                    Geef de schade aan
                </DialogTitle>
                <DialogContent>
                    <DamageForm damageType={damageType} initialValues={initialValues}/>
                </DialogContent>
                <DialogActions>
                    <Grid container justify={"flex-end"} style={{marginTop: 30}}>
                        <Button onClick={() => onCancel()} variant={"outlined"} color="default">
                            Cancel
                        </Button>
                        <Button disabled={!valid} onClick={() => onOk(
                                    {
                                        articleUuid: article.uuid,
                                        damage: {
                                            type: formData.values.damageType,
                                            code: formData.values.damageCode,
                                            remark: formData.values.remark
                                        }
                                    }
                                )}
                                variant={"raised"}
                                color={"primary"}>
                            Ok
                        </Button>
                    </Grid>
                </DialogActions>
            </Dialog>
    );
};

const selector = formValueSelector('damage');

const mapStateToProps = (state) => {
    const selectedArticle = findByArticleId(state.selectedDossier, state.selectedArticleId);
    return {
        formData: state.form.damage,
        initialValues: selectedArticle ? { damageType: selectedArticle.damage.type, remark: selectedArticle.damage.remark, damageCode: selectedArticle.damage.code} : null,
        article: selectedArticle,
        damageType: selector(state, 'damageType'),
        damageCode: selector(state, 'damageCode'),
    }
};

const mapDispatchToProps = () => ({
});

const DamageDialog = compose(
    connect(mapStateToProps, mapDispatchToProps),
    reduxForm({
        form: 'damage',
        enableReinitialize: true,
        validate: validator
    })
)(_DamageDialog);

export { DamageDialog };
