import {Radio, FormControlLabel, RadioGroup, withStyles} from "@material-ui/core";
import * as React from "react";

const styles = (theme) => {
    return {
        input: {
            width: "80%",
            display: 'flex',
            justifyContent: 'space-between',
            flexDirection: 'row'
        }
    }
};
export const MarksChoiceInput = withStyles(styles, {withTheme: true})(({input, label, meta: {touched, error}, ...custom}) => {
    const {classes} = custom;
    const hasError = () => {
        return !!(error) && touched;
    };
    return (
        <div>
                <RadioGroup 
                    aria-label="Marks"
                    name="marks"
                    className={classes.input}
                    value={input.value}
                    onChange={(e) => input.onChange(e.target.value)}
                >
                    <FormControlLabel value="NONE" control={<Radio color="primary"/>} label="Conform" />
                    <FormControlLabel value="OTHER" control={<Radio color="primary"/>} label="Anders" />
                </RadioGroup>
        </div>
    )
});
