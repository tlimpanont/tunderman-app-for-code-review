import {IconButton, Input, InputAdornment, TextField, withStyles} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import FormHelperText from "@material-ui/core/FormHelperText/FormHelperText";
import * as React from "react";

const styles = (theme) => {
    return {
        input: {
            width: "100%"
        }
    }
};
export const PalletTypesInput = withStyles(styles, {withTheme: true})(({input, label, meta: {touched, error}, ...custom}) => {
    const {classes} = custom;
    const palletTypes = [
        {
            value: null,
            label: null
        },
        {
            value: 'Blok',
            label: 'Blok',
        },
        {
            value: 'Euro',
            label: 'Euro',
        },
        {
            value: 'Origin',
            label: 'Origin',
        },
        {
            value: 'WWP',
            label: 'WWP',
        },
        {
            value: '2XE',
            label: '2XE',
        },
    ];

    const hasError = () => {
        return !!(error) && touched
    };
    return (
        <div>
            <TextField
                className={classes.input}
                select
                {...input}
                error={hasError()}
                helperText={(hasError()) ? error.message : ""}
                onChange={(e) => input.onChange(e.target.value)}
                SelectProps={{
                    native: true,
                }}
            >
                {palletTypes.map(option => (
                    <option key={option.value} value={option.value}>
                        {option.label}
                    </option>
                ))}
            </TextField>
        </div>

    )
});
