import api from '../../../api';

export const asyncFormValidators = (values, dispatch) => {
    return new Promise((resolve, reject) => {
        if (values.location) {
            api.get(`/api/locations?shortname=${values.location}`)
                .then((response) => {
                    //locatie wel gevonden
                })
                .catch((e) => {
                    reject({location: {message: "Geen geldige locatie"}});
                })
        }
    });
};