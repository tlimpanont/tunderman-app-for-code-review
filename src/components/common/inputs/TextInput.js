import {TextField, withStyles} from "@material-ui/core";
import * as React from "react";

const styles = (theme) => {
    return {
        input: {
            width: "100%"
        }
    }
};
export const TextInput = withStyles(styles, {withTheme: true})(({input, label, meta: {touched, error}, ...custom}) => {
    const {
        classes,
        onEnterPress = () => {
        },
        multiline,
        rows,
        rowsMax
    } = custom;
    const hasError = () => {
        return !!(error) && touched;
    };

    return (
        <div>
            <TextField className={classes.input}
                       {...input}
                       multiline={multiline}
                       rows={rows}
                       rowsMax={rowsMax}
                       onChange={(e) => input.onChange(e.target.value)}
                       onKeyPress={(e) => {
                           if (e.key === 'Enter') {
                               e.target.blur();
                               e.preventDefault();
                               onEnterPress();
                           }}}
                       error={hasError()}
                       helperText={(hasError()) ? error.message : ""}
            />
        </div>
    )
});
