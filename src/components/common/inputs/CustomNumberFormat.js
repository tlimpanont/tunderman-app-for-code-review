import NumberFormat from "react-number-format";
import * as React from "react";

export const CustomNumberFormat = (props) => {
    const {inputRef, onChange, ...other} = props;

    return (
        <NumberFormat
            {...other}
            onValueChange={values => {
                onChange({
                    target: {
                        floatValue: values.floatValue,
                        value: values.value,
                        formattedValue: values.formattedValue
                    },
                });
            }}
            thousandSeparator={"\."}
            decimalSeparator={"\,"}
        />
    );
}