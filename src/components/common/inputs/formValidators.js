export default function(values) {
    const errors = {};
    const requiredFields = [
        'palletType',
        'length',
        'width',
        'height'
    ];

    const mustNotBeZeroFields = [
        'palletCount',
        'articlesPerPallet',
        'length',
        'width',
        'height',
    ];

    mustNotBeZeroFields.forEach(field => {
        if (Number(values[field]) <= 0 ) {
            errors[field] = {message: "Getal moet groter zijn dan 0"}
        }
    });

    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = {message: 'Veld is verplicht'};
        }
    });

    return errors;
}
