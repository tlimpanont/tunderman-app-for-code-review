import {IconButton, Input, InputAdornment, withStyles} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import FormHelperText from "@material-ui/core/FormHelperText/FormHelperText";
import * as React from "react";

const styles = (theme) => {
    return {
        input: {
            width: "100%"
        }
    }
};
export const NumberInput = withStyles(styles, {withTheme: true})(({input, label, meta: {touched, error}, ...custom}) => {
    const {classes} = custom;
    const hasError = () => {
        return !!(error) && touched;
    };
    return (
        <div>
            <Input className={classes.input}
                   {...input}
                   error={hasError()}
                   onFocus={function (e) {
                       e.target.select()
                   }}
                   onKeyPress={(e) => {
                       if (e.key === 'Enter') {
                           e.target.blur();
                           e.preventDefault();
                       }}}
                   endAdornment={
                       <InputAdornment position="end">
                           <IconButton color={"default"}
                                       onClick={() => input.onChange(Number(input.value) + 1) }>
                               <AddIcon/>
                           </IconButton>
                       </InputAdornment>
                   }
            />
            {(hasError()) ? <FormHelperText error={hasError()}>{(hasError()) ? error.message : ""}</FormHelperText> : null}
        </div>
    )
});
