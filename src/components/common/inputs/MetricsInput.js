import {Input, InputAdornment, withStyles} from "@material-ui/core";
import FormHelperText from "@material-ui/core/FormHelperText/FormHelperText";
import * as React from "react";
import {CustomNumberFormat} from "./CustomNumberFormat";

const styles = (theme) => {
    return {
        input: {
            width: "100%"
        }
    }
};
export const MetricsInput = withStyles(styles, {withTheme: true})(({input, label, meta: {touched, error}, ...custom}) => {
    const {classes, metricUnit = "cm"} = custom;
    const hasError = () => {
        return !!(error) && touched;
    };
    return (
        <div>
            <Input className={classes.input}
                   {...input}
                   onFocus={function (e) {
                       e.target.select()
                   }}
                   onKeyPress={(e) => {
                       if (e.key === 'Enter') {
                           e.target.blur();
                           e.preventDefault();
                       }}}
                   onChange={(e) => input.onChange(e.target.floatValue)}
                   endAdornment={
                       <InputAdornment position="end">{metricUnit}</InputAdornment>
                   }
                inputComponent={CustomNumberFormat}
                   error={hasError()}
            />
            {(hasError()) ?
                <FormHelperText error={hasError()}>{(hasError()) ? error.message : ""}</FormHelperText> : null}
        </div>
    )
});
