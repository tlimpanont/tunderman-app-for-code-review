import {Grid, IconButton, Input, InputAdornment, TextField, withStyles} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import FormHelperText from "@material-ui/core/FormHelperText/FormHelperText";
import * as React from "react";
import Switch from "@material-ui/core/Switch/Switch";

const styles = (theme) => {
    return {
        input: {
            width: "100%"
        },
        switchBase: {
            '&$checked': {
                color: theme.palette.common.white,
                '& + $bar': {
                    backgroundColor: theme.palette.primary.main,
                },
            },
            transition: theme.transitions.create('transform', {
                duration: theme.transitions.duration.shortest,
                easing: theme.transitions.easing.sharp,
            }),
        },
        checked: {
            transform: 'translateX(15px)',
            '& + $bar': {
                opacity: 1,
                border: 'none',
            },
        },
        bar: {
            borderRadius: 13,
            width: 42,
            height: 26,
            marginTop: -13,
            marginLeft: -21,
            border: 'solid 1px',
            borderColor: theme.palette.grey[400],
            backgroundColor: theme.palette.grey[50],
            opacity: 1,
            transition: theme.transitions.create(['background-color', 'border']),
        },
        icon: {
            width: 24,
            height: 24,
        },
        iconChecked: {
            boxShadow: theme.shadows[1],
        },
    }
};
export const SwitchInput = withStyles(styles, {withTheme: true})(({input, label, meta: {touched, error}, ...custom}) => {
    const {classes} = custom;
    const hasError = () => {
        return !!(error) && touched;
    };
    console.log(input.value);
    return (
        <div>
            <Switch
                    checked={Boolean(input.value)}
                    value={input.value.toString()}
                    color={"primary"}
                    classes={{
                        switchBase: classes.switchBase,
                        bar: classes.bar,
                        icon: classes.icon,
                        iconChecked: classes.iconChecked,
                        checked: classes.checked,
                    }}
                    onChange={(e, checked) => input.onChange(Boolean(checked)) }
            />
        </div>
    )
});
