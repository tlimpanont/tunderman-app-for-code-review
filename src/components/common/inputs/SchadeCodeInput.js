import {MenuItem, withStyles, Select} from "@material-ui/core";
import * as React from "react";

const styles = (theme) => {
    return {
        input: {
            width: "100%"
        },
        item: {
            borderBottom: `0px`,
            borderColor: `#FFFFFF !important`
        }
    }
};
export const SchadeCodeInput = withStyles(styles, {withTheme: true})(({input, label, meta: {touched, error}, ...custom}) => {
    const {classes} = custom;
    const hasError = () => {
        return !!(error) && touched;
    };
    return (
        <div>
            <Select
                value={input.value}
                onChange={(e) => input.onChange(e.target.value)}
                displayEmpty
                name="age"
                className={classes.input}
            >
                <MenuItem className={classes.item} value=""/>
                <MenuItem className={classes.item} value={"DA1"}>DA1</MenuItem>
                <MenuItem className={classes.item} value={"DA2"}>DA2</MenuItem>
                <MenuItem className={classes.item} value={"MOL"}>MOL</MenuItem>
                <MenuItem className={classes.item} value={"COU"}>COU</MenuItem>
                <MenuItem className={classes.item} value={"PLT"}>PLT</MenuItem>
            </Select>
        </div>
    )
});
