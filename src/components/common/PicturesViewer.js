import {Card, CardContent, GridList, GridListTile, Button, withStyles, Grid} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import React from "react";
import {PhotoDialog} from "./PhotoDialog";

const styles = theme => {
    return {
        closeIcon: {
            float: "right",
            padding: 10,
            color: theme.palette.secondary.main
        }
    }
};
export const PicturesViewer = withStyles(styles, {withTheme: true})((props) => {
    const {pictures, cols, onRemovePicture, onSelectPicture, classes, children} = props;
    return (
        <GridList cols={(cols) ? cols : 6}>
            {pictures.map((url) => (
                <GridListTile key={url}>
                    <Card>
                        <CloseIcon className={classes.closeIcon}
                                   onClick={() => onRemovePicture(url)}
                        />
                        <CardContent>
                            <img src={process.env.API_BASE_URL + url} alt={url} style={{width: "100%"}} onClick={() => onSelectPicture(process.env.API_BASE_URL + url)}/>
                        </CardContent>
                    </Card>
                </GridListTile>
            ))}
            <Grid container direction={"column"} justify={"center"} alignItems={"center"}>
                <GridListTile>
                    {children}
                </GridListTile>
            </Grid>
        </GridList>
    )
});