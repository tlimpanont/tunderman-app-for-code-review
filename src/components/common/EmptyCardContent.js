import React from "react";
import {CardContent, Grid, Typography} from "@material-ui/core";
import {grey} from "@material-ui/core/colors";

export const EmptyCardContent = ({imageSource, label, imageWidth = 300}) => {
    return (
        <CardContent>
            <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
            >
                <img src={imageSource} alt="container" style={{width: imageWidth, opacity: 0.3, paddingBottom: 20}}/>
                <Typography variant="subheading" component={"h2"} style={{color: grey.A200}} align="center">
                    {label}
                </Typography>
            </Grid>
        </CardContent>
    )
};
