import React from "react";
import { Grid, Typography, withStyles} from "@material-ui/core";
import { MultiLineText } from ".";

const _LabelAndValue = ({label, value, labelSize = 6, minimumFractionDigits = 0}) => {

    return (
        <Grid container direction={"row"}>
            <Grid item xs={labelSize}>
                <Typography variant={"body1"} component={"label"} color={"primary"}>{label}</Typography>
            </Grid>
            <Grid item xs={12 - labelSize}>
                <Typography variant={"body1"} component={"label"}
                            color={"primary"}>
                    <strong>
                        {
                            value.toLocaleString(
                                'nl-NL',
                                {minimumFractionDigits: minimumFractionDigits}
                            )
                        }
                    </strong>
                </Typography>
            </Grid>
        </Grid>
    )
};

const styles = {
};

const LabelAndNumberValue = withStyles(styles)(_LabelAndValue);
export { LabelAndNumberValue };