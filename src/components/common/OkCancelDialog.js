import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';

function Transition(props) {
    return <Slide direction="down" {...props} />;
}

const OkCancelDialog = ({title, content, open, onCancel, onOk, okText = 'Ok', cancelText = 'Cancel'}) => {
    return (
            <Dialog
                open={open}
                TransitionComponent={Transition}
                keepMounted
                onClose={() => onCancel()}
            >
                <DialogTitle>
                    {title}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        {content}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button style={{width: 200}} onClick={() => onCancel()} variant={"outlined"} color="default">
                        {cancelText}
                    </Button>
                    <Button style={{width: 200}} onClick={() => onOk()} variant={"raised"} color="primary">
                        {okText}
                    </Button>
                </DialogActions>
            </Dialog>
    );
};

export { OkCancelDialog };
