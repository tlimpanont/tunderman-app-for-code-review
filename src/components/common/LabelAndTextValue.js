import React from "react";
import {Grid, Typography, withStyles} from "@material-ui/core";
import {MultiLineText} from ".";

const _LabelAndValue = ({label, value, labelSize = 6, expandWithCollapse = false, classes}) => {

    return (
        <Grid container direction={"row"}>
            <Grid item xs={labelSize}>
                <Typography variant={"body1"} component={"label"} color={"primary"}>{label}</Typography>
            </Grid>
            <Grid item xs={12 - labelSize}>
                <Typography variant={"body1"} component={"label"}
                            color={"primary"}>
                    <strong>
                        <MultiLineText value={value} maxNumberOfLines={5} expandWithCollapse={expandWithCollapse}/>
                    </strong>
                </Typography>
            </Grid>
        </Grid>
    )
};

const styles = {

};

const LabelAndTextValue = withStyles(styles)(_LabelAndValue);
export { LabelAndTextValue };