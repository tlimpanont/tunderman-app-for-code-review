import React from "react";
import {Grid, Typography, withStyles, IconButton} from "@material-ui/core";
import {MultiLineText} from ".";
import EditIcon from '@material-ui/icons/Edit';

const _MarksAndNumbers = ({value, newValue, labelSize = 6, onClick = () => {}, editMarksAndNumbers }) => {
    return (
        <Grid container direction={"row"}>
            <Grid item xs={labelSize}>
                <Typography variant={"body1"} component={"label"} color={"primary"}>Marks & Nrs:</Typography>
            </Grid>
            <Grid item xs={12 - labelSize - 1}>
                <Typography variant={"body1"} component={"label"}
                            color={"primary"}>
                    <strong>
                        <MultiLineText value={value} maxNumberOfLines={5} expandWithCollapse={true}/>
                    </strong>
                </Typography>
            </Grid>
            <Grid item xs={1}>
                <IconButton style={{marginTop: -12}} onClick={() => onClick()}
                    color={newValue?"secondary":"primary"} disabled={!editMarksAndNumbers}
                >
                    <EditIcon/>
                </IconButton>
            </Grid>
        </Grid>
    )
};

const styles = {

};

const MarksAndNumbers = withStyles(styles)(_MarksAndNumbers);
export { MarksAndNumbers };