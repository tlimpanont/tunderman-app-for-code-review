import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import {Grid} from "@material-ui/core";
import {reduxForm} from "redux-form";
import {connect} from "react-redux";
import {compose} from "recompose";
import {findByArticleId} from "../../actions-reducers";
import {asyncFormValidators} from "./inputs/asyncFormValidators";
import {LocationForm} from "../unload/LocationForm";

function Transition(props) {
    return <Slide direction="down" {...props} />;
}

const validator = (values) => {
    const errors = {};
    const requiredFields = [
        'location'
    ];

    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = {message: 'Veld is verplicht'};
        }
    });

    return errors;
};

const _LocationDialog = (props) => {
    const {open, onCancel, onOk, valid, formData, article} = props;

    return (
        <Dialog
            open={open}
            TransitionComponent={Transition}
            keepMounted
            onClose={() => onCancel()}
        >
            <DialogTitle>
                Voeg locatie toe voor alle losregels
            </DialogTitle>
            <DialogContent>
                <LocationForm onSubmit={() => onOk(
                    {
                        article: article,
                        location: formData.values.location
                    }
                )}/>
            </DialogContent>
            <DialogActions>
                <Grid container justify={"flex-end"} style={{marginTop: 30}}>
                    <Button onClick={() => onCancel()} variant={"outlined"} color="default">
                        Cancel
                    </Button>
                    <Button disabled={!valid} onClick={() => {
                        onOk(
                            {
                                article: article,
                                location: formData.values.location
                            }
                        );
                    }}
                            variant={"raised"}
                            color={"primary"}>
                        Ok
                    </Button>
                </Grid>
            </DialogActions>
        </Dialog>
    );
};

const mapStateToProps = (state) => {
    const selectedArticle = findByArticleId(state.selectedDossier, state.selectedArticleId);
    return {
        formData: state.form.location,
        article: selectedArticle
    }
};

const mapDispatchToProps = () => ({
});

const LocationDialog = compose(
    connect(mapStateToProps, mapDispatchToProps),
    reduxForm({
        form: 'location',
        enableReinitialize: true,
        asyncValidate: asyncFormValidators,
        asyncBlurFields: ['location'],
        validate: validator
    })
)(_LocationDialog);

export { LocationDialog };
