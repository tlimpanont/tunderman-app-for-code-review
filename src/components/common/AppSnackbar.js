import * as React from "react";
import {IconButton, Snackbar, withStyles} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import {compose} from 'recompose';
import {connect} from "react-redux";

class _AppSnackbar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: true
        }
    }

    handleClose = (event, reason) => {
        this.setState({open: false});
    };

    componentDidMount() {

    }

    render() {
        const {classes, appError} = this.props;
        return (
            <div>
                {(appError) ? (
                    <Snackbar
                        anchorOrigin={{
                            vertical: 'top',
                            horizontal: 'right',
                        }}
                        open={this.state.open}
                        autoHideDuration={6000}
                        onClose={this.handleClose}
                        ContentProps={{
                            'aria-describedby': 'message-id',
                        }}
                        message={<span id="message-id">
                            {
                                (appError.message)
                                    ? appError.message
                                    : "Onbekende fout opgetreden. Probeer het opnieuw"
                            }
                        </span>}
                        action={[
                            "",
                            <IconButton
                                key="close"
                                aria-label="Close"
                                color="inherit"
                                className={classes.close}
                                onClick={this.handleClose}
                            >
                                <CloseIcon/>
                            </IconButton>,
                        ]}
                    />
                ) : null}
            </div>
        )
    }
}

const styles = theme => ({
    close: {
        padding: theme.spacing.unit / 2,
    },
});

export const AppSnackbar = compose(
    withStyles(styles, {withTheme: true}),
    connect((state) => {
        return {
            appError: state.app.error
        }
    })
)(_AppSnackbar);