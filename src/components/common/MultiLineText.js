import React from "react";
import {Collapse} from "@material-ui/core";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';


const WithoutExpandWithCollapse = ({value, maxNumberOfLines}) => {
    return value ? value.toString().split('\n').slice(0, maxNumberOfLines).map((item, key) => {
        return <span key={key}>{item}<br/></span>
    }) : '';
};

const ExpandWithCollapse = ({value, expanded, maxNumberOfLines, handleExpandClick}) => {
    const splittedValue = value ? value.toString().split('\n') : [];
    if (splittedValue.length > maxNumberOfLines) {
        return (
            <div>
                <WithoutExpandWithCollapse value={value} maxNumberOfLines={maxNumberOfLines - 1}/>
                <Collapse in={expanded} timeout="auto" unmountOnExit>
                    {
                        splittedValue.slice(maxNumberOfLines - 1, Number.POSITIVE_INFINITY).map((item, key) => {
                            return <span key={key}>{item}<br/></span>
                        })
                    }
                </Collapse>
                {
                    (!expanded) ? (
                        <span style={{fontWeight: "normal"}} onClick={handleExpandClick}>Meer <ExpandMoreIcon/></span>
                    ) : <span style={{fontWeight: "normal"}} onClick={handleExpandClick}>Minder <ExpandLessIcon/></span>
                }
            </div>
        )
    } else {
        return <WithoutExpandWithCollapse value={value} maxNumberOfLines={maxNumberOfLines}/>
    }
};


class MultiLineText extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            expanded: false
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState(state => ({expanded: false}));
    }

    handleExpandClick = () => {
        this.setState(state => ({expanded: !state.expanded}));
    };

    render() {
        const {value = '', maxNumberOfLines = 10, expandWithCollapse} = this.props;

        return (
            (!expandWithCollapse)
                ? <WithoutExpandWithCollapse value={value} maxNumberOfLines={maxNumberOfLines}/>
                : <ExpandWithCollapse value={value}
                                      expanded={this.state.expanded}
                                      handleExpandClick={this.handleExpandClick}
                                      maxNumberOfLines={maxNumberOfLines}
                />

        )
    }

}

export {MultiLineText};