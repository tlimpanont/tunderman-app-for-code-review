import {AppBar, IconButton, Tab, Tabs, Toolbar, Typography} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import React from "react";
import {withRouter} from "react-router-dom";
import { Grid}  from "@material-ui/core";

class AppToolbarWithTabs extends React.Component {

    render() {
        const {history} = this.props;

        return (
            <AppBar position="fixed" color="primary">
                <Toolbar>
                    <Grid container justify={"center"}>
                        <Typography variant="title" color="inherit">
                            Tunderman App
                        </Typography>
                    </Grid>
                </Toolbar>
                <Tabs value={this.props.selectedTabIndex}>
                    <Tab label="Nieuwe loslijsten" onClick={() => history.replace('/')}/>
                    <Tab label="Onder handen" onClick={() => history.replace('/draft')}/>
                    <Tab label="Historie" onClick={() => history.replace('/history')}/>
                </Tabs>
            </AppBar>
        )
    }
}

export default withRouter(AppToolbarWithTabs);