import React, {PureComponent} from "react";
import {Button} from "@material-ui/core";
import CameraIcon from "@material-ui/icons/CameraAlt";

class HTMLCameraButton extends PureComponent {

    render() {
        const {
            onFileChange, onReaderError,
            onReaderAbort, variant, color,
            buttonText = "Foto",
            buttonIcon = <CameraIcon/>,
            onClick,
            children,
            disabled
        } = this.props;
        return (
            <div>
                <input
                    accept="image/*"
                    id="get-camera-button"
                    type="file"
                    onChange={(event) => {
                        const input = event.target;

                        const reader = new FileReader();
                        reader.onload = function () {
                            const dataURL = reader.result;
                            onFileChange(dataURL);
                        };
                        reader.onerror = onReaderError;
                        reader.onabort = onReaderAbort;
                        reader.readAsDataURL(input.files[0]);

                    }}
                    style={{display: "none"}}
                />
                <label htmlFor="get-camera-button">
                    {(!children)
                        ? <Button disabled={disabled} onClick={onClick} variant={variant} color={color} component="div"
                                  style={{width: 150, height: 48}}>
                            {buttonIcon} &nbsp; {buttonText}
                        </Button>
                        : <div key={Math.random() * 1000000}>{children}</div>
                    }
                </label>
            </div>
        )
    }
}

/**
 *  GetPictureButton with fallback to simple input file when developing on HTML Browser
 * @param onGetPictureSuccess: imageDataURL e.g. "data:image/jpeg;base64," + imageData
 * @param onGetPictureFail
 * @returns {*}
 * @constructor
 */
export class GetPictureButton extends PureComponent {

    click() {
        if (this.props.disabled) {
            return;
        }

        const {
            onClick = () => {
            }
        } = this.props;

        if (window.cordova && navigator.camera) {
            onClick.call(this);
            this.getPicture();
        }
    }

    getPicture() {
        const {
            onGetPictureSuccess,
            onGetPictureFail,
        } = this.props;

        navigator.camera.getPicture((imageData) => {
                onGetPictureSuccess(`data:image/jpeg;base64,${imageData}`);
            }, onGetPictureFail,
            {
                quality: 25,
                destinationType: Camera.DestinationType.DATA_URL,
                cameraDirection: Camera.Direction.BACK,
                correctOrientation: false
            });
    };

    render() {
        const {
            disabled = false,
            onGetPictureSuccess,
            onGetPictureFail,
            buttonText = "Foto",
            buttonIcon = <CameraIcon/>,
            onClick = () => {
            },
            children
        } = this.props;


        return (
            <div>
                {
                    (window.cordova && navigator.camera)
                        ? (
                            <Button ref={e => this.button = e} disabled={disabled} {...this.props} onClick={(e) => {
                                onClick.call(this, e);
                                this.getPicture();
                            }}>
                                {
                                    (!children)
                                        ? (<div>{buttonIcon} &nbsp;  {buttonText}</div>)
                                        : (<div>{children}</div>)
                                }
                            </Button>
                        )
                        : <HTMLCameraButton disabled={disabled} {...this.props} onFileChange={onGetPictureSuccess}
                                            onReaderError={onGetPictureFail}
                                            onReaderAbort={onGetPictureFail}>
                            {children}
                        </HTMLCameraButton>
                }

            </div>
        )
    }

}

