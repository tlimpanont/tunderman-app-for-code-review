import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import {Zoom} from "@material-ui/core";

function Transition(props) {
    return <Zoom in="true" {...props} />;
}

const PhotoDialog = (props) => {
    const {open, onClose, url} = props;
    
    return (
            <Dialog
                open={open}
                TransitionComponent={Transition}
                keepMounted
                onClose={() => onClose()}
            >
                <DialogContent>
                    <img src={url} alt={url} style={{width: "100%"}} onClick={() => onClose()}/>
                </DialogContent>
            </Dialog>
    );
};

export { PhotoDialog };
