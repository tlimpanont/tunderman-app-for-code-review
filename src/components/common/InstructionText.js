import React from "react";
import {Grid, Typography, withStyles} from "@material-ui/core";
import {MultiLineText} from ".";
import classnames from "classnames";

const _InstructionText = ({value, classes}) => {

    return (
        <Typography variant={"body1"} component={"label"}
                    color={"secondary"}
                    classes={{
                        colorSecondary: classnames({
                            [classes.colorSecondary]: value
                        })
                    }}
                   >
            <strong>
                <MultiLineText value={value} maxNumberOfLines={5}/>
            </strong>
        </Typography>
    )
};

const styles = (theme) => {
    return {
        colorSecondary: {
            border: `3px solid ${theme.palette.secondary.main}`,
            padding: 10,
            marginTop: 10,
            marginRight: 10
        }
    }
};

const InstructionText = withStyles(styles, {withTheme: true})(_InstructionText);
export { InstructionText };