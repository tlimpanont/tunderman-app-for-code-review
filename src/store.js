import {createStore, applyMiddleware} from "redux";
import createSagaMiddleware from 'redux-saga'
import reducers from './actions-reducers';
import {composeWithDevTools} from 'redux-devtools-extension';
import {
    articleDamageSaga,
    articleLinesSaga,
    articlePhotosSaga,
    articlesSaga,
    dossierPhotosSaga,
    dossiersSaga
} from "./sagas";

const composeEnhancers = composeWithDevTools({
    // Specify name here, actionsBlacklist, actionsCreators and other options if needed
});
const sagaMiddleware = createSagaMiddleware();

export default createStore(reducers, composeEnhancers(
    // options like actionSanitizer, stateSanitizer
    applyMiddleware(sagaMiddleware)
));

sagaMiddleware.run(dossiersSaga);
sagaMiddleware.run(dossierPhotosSaga);
sagaMiddleware.run(articlesSaga);
sagaMiddleware.run(articlePhotosSaga);
sagaMiddleware.run(articleLinesSaga);
sagaMiddleware.run(articleDamageSaga);