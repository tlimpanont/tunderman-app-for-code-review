import {createAction, handleActions} from 'redux-actions';

const initState = null;

// Action Creators
export const doSelectArticle = createAction('SELECT_ARTICLE');
export const doResetArticleSelection = createAction('RESET_ARTICLE_SELECTION');

// Reducers
export const articleSelectionReducer = handleActions(
    {
        [doSelectArticle]: (state, { payload } ) => {
            return payload;
        },
        [doResetArticleSelection]: () => {
            return initState;
        }
    },
    initState
);

// Selectors