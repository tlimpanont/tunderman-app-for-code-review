import { createAction, handleActions } from 'redux-actions';

const initState = null;

// Action Creators
export const setFilter = createAction('SET_FILTER');
export const resetFilter = createAction('RESET_FILTER');

// Reducers
export const filterReducer = handleActions(
    {
        [setFilter]: (state, { payload } ) => {
            return payload;
        },
        [resetFilter]: () => {
            return null;
        }
    },
    initState
);

// Selectors