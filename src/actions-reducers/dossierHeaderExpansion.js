import {createAction, handleActions} from 'redux-actions';
import {doSelectArticle} from "./articleSelection";

const initState = true;

// Action Creators
export const doExpandDossierHeader = createAction('EXPAND_DOSSIER_HEADER');
export const doCollapseDossierHeader = createAction('COLLAPSE_DOSSIER_HEADER');

// Reducers
export const dossierHeaderExpansionReducer = handleActions(
    {
        [doExpandDossierHeader]: () => {
            return true;
        },
        [doCollapseDossierHeader]: () => {
            return false;
        },
        [doSelectArticle]: () => {
            return false;
        }
    },
    initState
);

// Selectors