import {createAction, handleActions} from 'redux-actions';

const initState = {
    loading: false,
    error: null

};

// Action Creators
export const putErrorInStore = createAction("PUT_ERROR_IN_STORE");
export const apiIsFetching = createAction("API_IS_FETCHING");
export const apiFetchingIsCompleted = createAction("API_FETCHING_IS_COMPLETED");

// Reducers
export const appReducer = handleActions(
    {
        [putErrorInStore]: (state, {payload}) => {
            return {
                loading: false,
                error: payload
            }
        },
        [apiIsFetching]: (state, {payload}) => {
            return {
                loading: true,
                error: null
            }
        },
        [apiFetchingIsCompleted]: (state, {payload}) => {
            return {
                loading: false,
                error: null
            }
        }
    },
    initState
);

// Selectors