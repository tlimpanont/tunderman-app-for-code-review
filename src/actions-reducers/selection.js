import {createAction, handleActions} from 'redux-actions';

const initState = null;

// Action Creators
export const selectDossierNumber = createAction('SELECT_DOSSIER_NUMBER');
export const resetDossierNumberSelection = createAction('RESET_DOSSIER_NUMBER_SELECTION');

// Reducers
export const selectionReducer = handleActions(
    {
        [selectDossierNumber]: (state, { payload } ) => {
            return payload;
        },
        [resetDossierNumberSelection]: () => {
            return initState;
        }
    },
    initState
);

// Selectors