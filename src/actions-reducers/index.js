import {combineReducers} from "redux";
import {dossiersReducer} from './dossiers';
import {selectionReducer} from './selection';
import {filterReducer} from './filter';
import {dossierHeaderExpansionReducer} from './dossierHeaderExpansion';
import {articleSelectionReducer} from "./articleSelection";
import {selectionDossierReducer} from "./selectionDossier";
import {appReducer} from "./appReducer";
import {reducer as reduxFormReducer} from 'redux-form';
import {articleHeaderExpansionReducer} from "./articleHeaderExpansion";

export * from './dossiers';
export * from './selection';
export * from './filter';
export * from './dossierHeaderExpansion';
export * from './articleHeaderExpansion';
export * from './articleSelection';
export * from './selectionDossier';
export * from './appReducer';


export default combineReducers({
    app: appReducer,
    dossiers: dossiersReducer,
    selectedDossierNumber: selectionReducer,
    selectedDossier: selectionDossierReducer,
    selectedArticleId: articleSelectionReducer,
    filter: filterReducer,
    expandDossierHeader: dossierHeaderExpansionReducer,
    expandArticleHeader: articleHeaderExpansionReducer,
    form: reduxFormReducer
});
