import {createAction, handleActions} from 'redux-actions';
import * as dotDrop from "dot-prop-immutable";

const initState = null;

// Action Creators
export const selectDossier = createAction('SELECT_DOSSIER');
export const fetchDossierSuccess = createAction('FETCH_DOSSIER_SUCCESS');
export const addPictureToDossier = createAction('ADD_PICTURE_TO_DOSSIER');
export const addPictureToDossierSuccess = createAction('ADD_PICTURE_TO_DOSSIER_SUCCESS');
export const removePictureFromDossier = createAction('REMOVE_PICTURE_FROM_DOSSIER');
export const removePictureFromDossierSuccess = createAction('REMOVE_PICTURE_FROM_DOSSIER_SUCCESS');
export const addPictureToArticle = createAction('ADD_PICTURE_TO_ARTICLE');
export const addPictureToArticleSuccess = createAction('ADD_PICTURE_TO_ARTICLE_SUCCESS');
export const addDamageToArticle = createAction('ADD_DAMAGE_TO_ARTICLE');
export const addDamageToArticleSuccess = createAction('ADD_DAMAGE_TO_ARTICLE_SUCCESS');
export const addNewMarksAndNumbersToArticle = createAction('ADD_MARKS_AND_NUMBERS_TO_ARTICLE');
export const addNewMarksAndNumbersToArticleSuccess = createAction('ADD_MARKS_AND_NUMBERS_TO_ARTICLE_SUCCESS');
export const removePictureFromArticle = createAction('REMOVE_PICTURE_FROM_ARTICLE');
export const removePictureFromArticleSuccess = createAction('REMOVE_PICTURE_FROM_ARTICLE_SUCCESS');
export const finishUnloadArticle = createAction('FINISH_UNLOAD_ARTICLE');
export const finishUnloadArticleSuccess = createAction('FINISH_UNLOAD_ARTICLE_SUCCESS');
export const startUnloadingDossier = createAction('START_UNLOADING_DOSSIER');
export const startUnloadingDossierSuccess = createAction('START_UNLOADING_DOSSIER_SUCCESS');
export const saveAndExportDossier = createAction('SAVE_AND_EXPORT_DOSSIER');
export const saveAndExportDossierSuccess = createAction('SAVE_AND_EXPORT_DOSSIER_SUCCESS');
export const addLineToArticle = createAction('ADD_LINE_TO_ARTICLE');
export const addLineToArticleSuccess = createAction('ADD_LINE_TO_ARTICLE_SUCCESS');
export const editArticleLine = createAction('EDIT_ARTICLE_LINE');
export const editArticleLineSuccess = createAction('EDIT_ARTICLE_LINE_SUCCESS');
export const deleteLineFromArticle = createAction('DELETE_LINE_FROM_ARTICLE');
export const deleteLineFromArticleSuccess = createAction('DELETE_LINE_FROM_ARTICLE_SUCCESS');

// Reducers
export const selectionDossierReducer = handleActions(
    {
        [fetchDossierSuccess]: (state, {payload}) => {
            return payload;
        },
        [addPictureToDossierSuccess]: (state, {payload}) => {
            return payload;
        },
        [removePictureFromDossierSuccess]: (state, {payload}) => {
            return payload;
        },
        [startUnloadingDossierSuccess]: (state, {payload}) => {
            return payload;
        },
        [saveAndExportDossierSuccess]: (state, {payload}) => {
            return payload;
        },
        [addPictureToArticleSuccess]: (state, {payload}) => {
            const articleIndex = state.articles.findIndex(article => article.uuid === payload.uuid);
            return dotDrop.set(
                state,
                `articles.${articleIndex}`,
                payload
            );
        },
        [removePictureFromArticleSuccess]: (state, {payload}) => {
            const articleIndex = state.articles.findIndex(article => article.uuid === payload.uuid);
            return dotDrop.set(
                state,
                `articles.${articleIndex}`,
                payload
            );
        },
        [finishUnloadArticleSuccess]: (state, {payload}) => {
            const articleIndex = state.articles.findIndex(article => article.uuid === payload.uuid);
            return dotDrop.set(
                state,
                `articles.${articleIndex}`,
                payload
            );
        },
        [addLineToArticleSuccess]: (state, {payload}) => {
            const articleIndex = state.articles.findIndex(article => article.uuid === payload.uuid);
            return dotDrop.set(
                state,
                `articles.${articleIndex}.lines`,
                payload.lines
            );
        },
        [deleteLineFromArticleSuccess]: (state, {payload}) => {
            const articleIndex = state.articles.findIndex(article => article.uuid === payload.uuid);
            return dotDrop.set(
                state,
                `articles.${articleIndex}.lines`,
                payload.lines
            );
        },
        [editArticleLineSuccess]: (state, {payload}) => {

            const {updatedArticle, updatedArticleLine} = payload;

            const article = state.articles.find(article => article.uuid === updatedArticle.uuid);
            const articleIndex = state.articles.findIndex(article => article.uuid === payload.updatedArticle.uuid);
            const articleLineIndex = article.lines.findIndex(line => line.uuid === updatedArticleLine.uuid);

            return dotDrop.set(
                state,
                `articles.${articleIndex}.lines.${articleLineIndex}`,
                updatedArticleLine
            );
        },
        [addDamageToArticleSuccess]: (state, {payload}) => {
            const articleIndex = state.articles.findIndex(article => article.uuid === payload.uuid);
            return dotDrop.set(
                state,
                `articles.${articleIndex}`,
                payload
            );
        },
        [addNewMarksAndNumbersToArticleSuccess]: (state, {payload}) => {
            const articleIndex = state.articles.findIndex(article => article.uuid === payload.uuid);
            return dotDrop.set(
                state,
                `articles.${articleIndex}`,
                payload
            );
        },
    },
    initState
);

// Selectors