import {createAction, handleActions} from 'redux-actions';

const initState = [];

// Action Creators
export const fetchFilteredDossiers = createAction('FETCH_FILTERED_DOSSIERS');
export const fetchFilteredDossiersSuccess = createAction('FETCH_FILTERED_DOSSIERS_SUCCESS');

// Reducers
export const dossiersReducer = handleActions(
    {
        [fetchFilteredDossiersSuccess]: (state, action) => {
            return action.payload
        }
    },
    initState
);

//Selectors
export const findByDossierNumber = (dossiers, dossierNumber) => {
    return (dossierNumber) ?
        dossiers.find((dossier) => dossier.uuid === dossierNumber)
        : null
};

export const findByState = (dossiers, state) => {
    return dossiers.filter((dossier) => dossier.state.toLowerCase() === state.toLowerCase());
};

export const filterDossiers = (dossiers, filter) => {
    return dossiers.filter((dossier) => {
        return !filter
            || dossier.header.dossierNumber.toString().indexOf(filter) !== -1
            || dossier.header.container.toString().toLowerCase().indexOf(filter.toLowerCase()) !== -1
    });
};

export const findByArticleId = (dossier, articleId) => {
    if (!dossier) {
        return null;
    }
    return dossier.articles.find((article) => article.uuid === articleId);
};