import {createAction, handleActions} from 'redux-actions';
import {doSelectArticle} from "./articleSelection";

const initState = false;

// Action Creators
export const doExpandArticleHeader = createAction('EXPAND_ARTICLE_HEADER');
export const doCollapseArticleHeader = createAction('COLLAPSE_ARTICLE_HEADER');

// Reducers
export const articleHeaderExpansionReducer = handleActions(
    {
        [doExpandArticleHeader]: () => {
            return true;
        },
        [doCollapseArticleHeader]: () => {
            return false;
        },
        [doSelectArticle]: () => {
            return true;
        }
    },
    initState
);

// Selectors