import axios from "axios";
import store from "../store";
import {apiFetchingIsCompleted, apiIsFetching, putErrorInStore} from "../actions-reducers";

export const ACCESS_TOKEN_KEY = "tundermanAccessToken";

const instance = axios.create({
    baseURL: process.env.API_BASE_URL,
    timeout: 10000,
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
});

instance.interceptors.request.use((config) => {
    // store.dispatch(apiIsFetching());
    // check if localStorage or in memory has Bearer token, if so send with request
    const token = window.localStorage.getItem(ACCESS_TOKEN_KEY);
    return (token)
        ? {...config, headers: {'Authorization': `Bearer ${token}`}}
        : config;
}, (error) => {
    // Do something with request error
    store.dispatch(putErrorInStore(error));
    return Promise.reject(error);
});

instance.interceptors.response.use((response) => {
    // store.dispatch(apiFetchingIsCompleted());
    if (/auth\/login/.test(response.config.url) && /[A-Za-z0-9\-\._~\+\/]+=*/g.test(response.data.token)) {
        window.localStorage.setItem(ACCESS_TOKEN_KEY, response.data.token);
    }
    return response;
}, (error) => {
    // Do something with response error
    store.dispatch(putErrorInStore(error));
    return Promise.reject(error);
});

// automatic login and get token!, remove when login with form
instance.post("/api/auth/login", {
    username: process.env.AUTH_USER_NAME,
    password: process.env.AUTH_PASSWORD,
}).then((response) => {
});


export default instance;

