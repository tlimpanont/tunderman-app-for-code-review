import {createMuiTheme} from "@material-ui/core";

/*
link to material palette https://material.io/tools/color/#!/?view.left=0&view.right=1&primary.color=005baa&secondary.color=f04124
https://codepen.io/pen/
 */

export const palette = {
    primary: {
        light: "#5387dc",
        main: "#005baa",
        dark: "#00337a",
        contrastText: "#fff"
    },
    secondary: {
        light: "#ff7650",
        main: "#f04124",
        dark: "#b50000",
        contrastText: "#000"
    },
    appBackground: "rgba(0, 91, 170, 0.3)",
    appBackgroundHex: "#76a2cd",
};

export const tundermanDefaultTheme = createMuiTheme({
    overrides: {
        MuiListItem: {
            root: {
                borderBottom: `1px solid`,
                borderColor: `${palette.primary.main} !important`
            }
        },
        MuiListItemIcon: {
            root: {
                color: '#54C242'
            }
        },
        MuiListItemText: {
            primary: {
                color: palette.primary.main
            },
            secondary: {
                color: palette.primary.light
            }
        },
        MuiIconButton: {
            root: {
                color: palette.primary.main
            }
        },
        MuiInput: {
            input: {
                color: palette.primary.main,
                fontSize: 20
            },
            underline: {
                borderColor: palette.primary.main
            }
        },
        MuiBadge: {
            colorSecondary: {
                color: "#FFF"
            }
        },
        MuiButton: {
            root: {
                color: palette.primary.main,
                height: "48px",
                width: "158px",
                marginRight: 8,
                marginTop: 10,
            },
            outlined: {
                color: palette.primary.main,
                borderColor: palette.primary.main,
                borderWidth: 2
            },
            raised: {
                backgroundColor: "transparent",
                color: palette.primary.main
            },
            raisedPrimary: {
                backgroundColor: palette.primary.main,
                color: "white"
            },
            disabled: {
                borderColor: 'rgba(116, 134, 154, 0.5)',
            }
        },
        MuiCard: {
            root: {
                "margin": 10
            }
        },
        MuiPaper: {
            rounded: {
                borderRadius: "none"
            },
        },
        MuiCardHeader: {
            root: {
                "&.__has-border": {
                    borderBottom: `1px solid ${palette.primary.main}`,
                }
            },
            title: {
                color: `${palette.primary.main}`,
                fontWeight: "bolder",
                fontSize: 24
            },
            subheader: {
                color: `${palette.primary.light}`,
                fontWeight: "500"
            }
        },
        MuiDialogContentText: {
            root: {
                color: `${palette.primary.main} !important`,
                fontSize: 20
            }
        }
    },
    palette,
    typography: {
        fontSize: 20,
        title: {
            color: `${palette.primary.main}`,
            fontWeight: "bolder",
            fontSize: 24
        },
        subheading: {
            fontSize: 20
        },
        body1: {
            color: `${palette.primary.main}`,
        }
    }
});