import React from "react";
import {connect} from "react-redux";
import {Card, CardContent, CardHeader} from "@material-ui/core";
import {EmptyCardContent, LabelAndNumberValue, LabelAndTextValue} from "../../components/common";
import {findByDossierNumber} from "../../actions-reducers";

function showDossierHeaderDetail(selectedDossier) {
    const {
        dossierNumber, sender, refCustomer, unloadingDate,
        container, sealNumber, shipName, mrn, colli, weight, cbm
    } = selectedDossier.header;

    return (
        <CardContent>
            <LabelAndTextValue label='Zender:' value={sender}/>
            <LabelAndTextValue label='Ref Klant:' value={refCustomer}/>
            <LabelAndTextValue label='MRN:' value={mrn}/>
            <LabelAndTextValue label='Losdatum:' value={unloadingDate}/>
            <LabelAndTextValue label='Sealnr:' value={sealNumber}/>
            <LabelAndTextValue label='Bootnaam:' value={shipName}/>
            <LabelAndTextValue label='Dossiernummer:' value={dossierNumber}/>
            <LabelAndNumberValue label='Colli:' value={colli}/>
            <LabelAndNumberValue label='Gewicht:' value={weight}/>
            <LabelAndNumberValue label='CBM:' value={cbm} minimumFractionDigits={3}/>
        </CardContent>
    );
}

function showNoDossier() {
    return (<EmptyCardContent
        imageSource="assets/container_icon-300x300.png"
        label="Geen dossier geselecteerd"/>);
}

const _DossierHeaderCard = ({selectedDossier, style}) => {
    return (
        <Card style={style}>
            <CardHeader
                title={ selectedDossier ? 'Container: ' + selectedDossier.header.container : 'Container: <Geen>'}
                className={"__has-border"}
            />
            {selectedDossier ? showDossierHeaderDetail(selectedDossier) : showNoDossier()}
        </Card>);
};

const mapStateToProps = ({dossiers, selectedDossierNumber}) => {
    return {
        selectedDossier: findByDossierNumber(dossiers, selectedDossierNumber)
    }
};

const mapDispatchToProps = (dispatch) => ({
    onStartUnloading: (dossierNumber) => {
    }
});

const DossierHeaderCard = connect(mapStateToProps, mapDispatchToProps)(_DossierHeaderCard);

export {DossierHeaderCard};