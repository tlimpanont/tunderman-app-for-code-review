import React from 'react';
import {
    filterDossiers,
    findByState,
    resetFilter,
    selectDossierNumber,
    setFilter
} from "../../actions-reducers";
import { connect } from "react-redux";
import { DossierSearchList, SearchBox} from "../../components/search";
import { CardContent, Card } from "@material-ui/core";

const _DossierSearchListCard = (props) => {
    const { dossiers, selectedDossierNumber, filter, onFilter, onResetFilter, onSelect, style } = props;

    return (
        <Card style={style}>
            <SearchBox style={{position: "absolute", top: 0, left: 0, right: 0}}
                value={filter}
                onChangeText={(text) => onFilter(text)}
                onReset={() => onResetFilter()}/>
            <CardContent style={{padding: 0, position: "absolute", top: 89, bottom: 0, left: 0, right: 0, overflow: "scroll"}}>
                <DossierSearchList
                    data={dossiers}
                    selectedItem={selectedDossierNumber}
                    onSelect={(dossierNumber) => onSelect(dossierNumber)}/>
            </CardContent>
        </Card>
    );
};

const mapStateToProps = ({dossiers, selectedDossierNumber, filter}, {filterField}) => {
    return {
        dossiers: filterDossiers(findByState(dossiers, filterField), filter),
        selectedDossierNumber,
        filter: filter
    }
};

const mapDispatchToProps = (dispatch) => ({
    onSelect: (dossierNumber) => dispatch(selectDossierNumber(dossierNumber)),
    onFilter: (filter) => dispatch(setFilter(filter)),
    onResetFilter: () => dispatch(resetFilter())
});

const DossierSearchListCard = connect(mapStateToProps, mapDispatchToProps)(_DossierSearchListCard);

export { DossierSearchListCard };