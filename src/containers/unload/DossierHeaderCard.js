import React from 'react';
import {Grid, withStyles, Button, Typography} from "@material-ui/core";
import {ExpandableCard, LabelAndTextValue} from "../../components/common";
import {
    doCollapseDossierHeader,
    doExpandDossierHeader,
    findByDossierNumber
} from "../../actions-reducers";
import {connect} from "react-redux";
import {GetPictureButton} from "../../components/common/GetPictureButton";
import {PicturesViewer} from "../../components/common/PicturesViewer";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import CameraIcon from "@material-ui/icons/CameraAlt";
import {addPictureToDossier, removePictureFromDossier} from "../../actions-reducers/selectionDossier";

const styles = (theme) => {
    return {}
};

function renderButtons(dossier, addPictureToDossier, startUnloadingDossier) {
    if (!dossier) {
        return null;
    }

    if (dossier.state === 'NEW') {
        return (
            <Button style={{width: 250}}
                    variant={"raised"} color={"primary"}
                    onClick={() => {
                        startUnloadingDossier()
                    }}
                    >
                <PlayArrowIcon/>&nbsp; Start lossen
            </Button>
        );
    } else {
        return (
            <GetPictureButton
                buttonIcon={<CameraIcon/>}
                buttonText={"Foto"}
                variant={"raised"}
                color={"primary"}
                onGetPictureSuccess={(dataURL) => {
                    addPictureToDossier({
                        uuid: dossier.uuid,
                        dataURL: dataURL
                    })
                }}/>
        );
    }
}

function renderDossierHeader(dossierHeader) {
    if (!dossierHeader) {
        return null;
    }

    const {
        dossierNumber,
        sender,
        refCustomer,
        mrn,
        unloadingDate,
        container,
        sealNumber,
        shipName
    } = dossierHeader;

    return <Grid container
                 direction={"row"}
    >
        <Grid item xs={6}>
            <LabelAndTextValue label='Zender:' value={sender}/>
            <LabelAndTextValue label='Ref Klant:' value={refCustomer}/>
            <LabelAndTextValue label='MRN:' value={mrn}/>
            <LabelAndTextValue label='Losdatum:' value={unloadingDate}/>
        </Grid>
        <Grid item xs={6}>
            <LabelAndTextValue label='Sealnr:' value={sealNumber}/>
            <LabelAndTextValue label='Bootnaam:' value={shipName}/>
            <LabelAndTextValue label='Dossiernummer:' value={dossierNumber}/>
        </Grid>
    </Grid>;
}

const _DossierHeaderCard = ({
                                dossier, expanded = true, doCollapseDossierHeader,
                                doExpandDossierHeader, addPictureToDossier, startUnloadingDossier,
                                pictures, removePictureFromDossier, selectPicture, styles
                            }) => {
    return (
        <ExpandableCard
            title={dossier ? 'Container: ' + dossier.header.container : 'Container:'}
            expanded={expanded}
            onCollapse={doCollapseDossierHeader}
            onExpand={doExpandDossierHeader}
            styles={styles}
        >

            {renderDossierHeader(dossier ? dossier.header : null)}

            {
                (pictures.length <= 0) ?
                    (
                        <Grid container
                              direction={"row"} justify={"flex-end"}>
                            {renderButtons(dossier, addPictureToDossier, startUnloadingDossier)}
                        </Grid>
                    ) : null
            }
            <Grid container style={{marginTop: 20}}
                  direction={"row"} justify={"flex-start"}>
                <PicturesViewer pictures={pictures}
                                onRemovePicture={(picture) => {
                                    removePictureFromDossier(picture)
                                }}
                                onSelectPicture={(picture) => selectPicture(picture)}
                >
                    {
                        (pictures.length > 0)
                         ? <GetPictureButton
                                onGetPictureSuccess={(dataURL) => {
                                    addPictureToDossier({
                                        uuid: dossier.uuid,
                                        dataURL: dataURL
                                    })
                                }}>
                                {(pictures.length > 0) ? (
                                    <div style={{textAlign: "center"}}>
                                        <CameraIcon color={"primary"}/>
                                        <Typography variant={"body1"} color={"primary"}>Voeg foto toe</Typography>
                                    </div>
                                ) : null}
                            </GetPictureButton> : null
                    }

                </PicturesViewer>
            </Grid>

        </ExpandableCard>
    );
};

const mapStateToProps = ({dossiers, selectedDossierNumber, expandDossierHeader, selectedDossier}) => {
    const lessInfoSelectedDossier = findByDossierNumber(dossiers, selectedDossierNumber);
    return {
        dossier: (selectedDossier) ? selectedDossier : lessInfoSelectedDossier,
        expanded: expandDossierHeader,
        pictures: (selectedDossier) ? selectedDossier.header.pictures : []
    }
};


const DossierHeaderCard = connect(mapStateToProps, {
    doCollapseDossierHeader,
    doExpandDossierHeader,
    addPictureToDossier,
    removePictureFromDossier
})(
    withStyles(styles, {withTheme: true})(_DossierHeaderCard)
);

export {DossierHeaderCard};