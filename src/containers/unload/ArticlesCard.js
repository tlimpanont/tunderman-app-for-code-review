import React from 'react';
import {ArticleList} from "../../components/unload";
import {doSelectArticle} from "../../actions-reducers";
import {connect} from "react-redux";
import {Card, CardHeader} from "@material-ui/core";

const styles = {
    cardHeaderStyle: {
        position: "absolute",
        top: 0,
        left: 0,
        right: 0
    },
    cardContentStyle: {
        padding: 0,
        position: "absolute",
        top: 65,
        bottom: 0,
        left: 0,
        right: 0,
        overflow: "scroll"
    }
};

const _ArticlesCard = ({articleList, selectedArticleId, onSelect, style}) => {
    return (
        <Card style={style}>
            <CardHeader title="Artikelen" className={"__has-border"}
                        style={styles.cardHeaderStyle}/>
            <ArticleList
                data={articleList}
                selectedArticleId={selectedArticleId}
                onSelect={(articleId) => {onSelect(articleId)}}
                style={styles.cardContentStyle}/>
        </Card>
    );
};

const mapStateToProps = ({dossiers, selectedArticleId, selectedDossier}) => {
    return {
        articleList: (selectedDossier && selectedDossier.articles) ? selectedDossier.articles : [],
        selectedArticleId: selectedArticleId
    }
};

const mapDispatchToProps = (dispatch) => ({
    onSelect: (articleId) => dispatch(doSelectArticle(articleId))
});

const ArticlesCard = connect(mapStateToProps, mapDispatchToProps)(_ArticlesCard);

export { ArticlesCard };