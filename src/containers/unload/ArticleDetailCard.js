import React, {PureComponent} from 'react';
import {connect} from "react-redux";
import {Button, Card, CardContent, Grid, Typography, Badge} from "@material-ui/core";
import {
    addDamageToArticle,
    doResetArticleSelection, editArticleLine,
    findByArticleId,
    finishUnloadArticle
} from "../../actions-reducers";
import {GetPictureButton} from "../../components/common/GetPictureButton";
import {PicturesViewer} from "../../components/common/PicturesViewer";
import AddIcon from "@material-ui/icons/Add";
import CheckIcon from "@material-ui/icons/Check";
import {EmptyCardContent, DamageDialog, OkDialog, LocationDialog} from "../../components/common";
import {OkCancelDialog} from "../../components/common/OkCancelDialog";
import {Link, withRouter} from "react-router-dom";
import {ArticleLinesTable} from "../../components/unload/ArticleLinesTable";
import {addPictureToArticle, removePictureFromArticle} from "../../actions-reducers/selectionDossier";
import CameraIcon from "@material-ui/icons/CameraAlt";
import {reset} from 'redux-form';

class _ArticleDetailCard extends PureComponent {

    constructor(props) {
        super(props);
        this.showDialogFinishWithManco = this.showDialogFinishWithManco.bind(this);
        this.hideDialogFinishWithManco = this.hideDialogFinishWithManco.bind(this);
        this.handleFinishWithManco = this.handleFinishWithManco.bind(this);
        this.handleCancelFinishWithManco = this.handleCancelFinishWithManco.bind(this);
        this.handlePrepareFinish = this.handlePrepareFinish.bind(this);
        this.openDamageDialog = this.openDamageDialog.bind(this);
        this.hideDamageDialog = this.hideDamageDialog.bind(this);
        this.openLocationDialog = this.openLocationDialog.bind(this);
        this.hideLocationDialog = this.hideLocationDialog.bind(this);
        this.openMissingLocationsDialog = this.openMissingLocationsDialog.bind(this);
        this.closeMissingLocationsDialog = this.closeMissingLocationsDialog.bind(this);
        this.state = {
            showDialogFinishWithManco: false,
            showDamageDialog: false,
            showDialogMissingLocations: false,
            showLocationDialog: false
        };
    }

    showDialogFinishWithManco() {
        this.setState({showDialogFinishWithManco: true});
    }

    hideDialogFinishWithManco() {
        this.setState({showDialogFinishWithManco: false});
    }

    openDamageDialog() {
        this.setState({showDamageDialog: true});
    }

    hideDamageDialog() {
        this.setState({showDamageDialog: false});
    }

    openLocationDialog() {
        this.setState({showLocationDialog: true});
    }

    hideLocationDialog() {
        this.setState({showLocationDialog: false});
        this.props.resetForm('location');
    }

    openMissingLocationsDialog() {
        this.setState({showDialogMissingLocations: true});
    }

    closeMissingLocationsDialog() {
        this.setState({showDialogMissingLocations: false});
    }

    handleFinishWithManco() {
        const {article, onFinish} = this.props;
        this.hideDialogFinishWithManco();
        onFinish(article);
    }

    handleCancelFinishWithManco() {
        this.hideDialogFinishWithManco();
    }

    handlePrepareFinish() {
        const {article, onFinish} = this.props;

        if (this.missingLocations(article)) {
            this.openMissingLocationsDialog();
            return;
        }

        if (article.lines && this.numberOfArticlesUnloaded(article) >= article.colli) {
            onFinish(article);
        } else {
            this.showDialogFinishWithManco();
        }
    }

    numberOfArticlesUnloaded(article) {
        return article.lines.reduce((total, line) => {
            return total + (line.palletCount * line.articlesPerPallet)
        }, 0);
    }

    missingLocations(article) {
        return article.lines && article.lines.reduce((missingLocations, line) => {
            return missingLocations || !line.location
        }, false);
    }

    render() {
        const {style} = this.props;

        return (
            <Card style={style}>
                {this.renderCardContent({...this.props})}
            </Card>
        );
    }

    renderCardContent({article, dossier, pictures, onRemovePicture, onAddPicture, onSelectPicture, onAddDamage, onAddLocation}) {
        if (!article) {
            return (
                <EmptyCardContent
                    imageSource="assets/pallet.png"
                    imageWidth={150}
                    label="Geen artikel geselecteerd"
                />)
        }

        return (<CardContent
            style={{padding: 0, position: "absolute", top: 0, bottom: 0, left: 0, right: 0, overflow: "scroll"}}>
            <OkCancelDialog
                title={'Artikel nog niet volledig gelost'}
                content={'Het aantal dat is gelost komt niet overeen met het opgegeven aantal. ' +
                'Wilt u dit artikel toch markeren als gelost? De ontbrekende artikelen zullen ' +
                'als manco worden opgegeven.'}
                open={this.state.showDialogFinishWithManco}
                onCancel={this.handleCancelFinishWithManco}
                onOk={this.handleFinishWithManco}/>
            <OkDialog
                title={'Ontbrekende locaties'}
                content={'Nog niet alle losregels zijn voorzien van een locatie. ' +
                'Vul bij alle regels de locatie in.'}
                open={this.state.showDialogMissingLocations}
                onOk={this.closeMissingLocationsDialog}/>
            <DamageDialog
                open={this.state.showDamageDialog}
                onOk={(data) => {
                    this.hideDamageDialog();
                    onAddDamage(data)
                }}
                onCancel={this.hideDamageDialog}
            />
            <LocationDialog
                open={this.state.showLocationDialog}
                onOk={(data) => {
                    this.hideLocationDialog();
                    onAddLocation(data)
                }}
                onCancel={this.hideLocationDialog}
            />
            <Grid container>
                <ArticleLinesTable rows={article.lines} onSelectRowItem={this.onSelectRowItem.bind(this)}/>
            </Grid>
            {this.renderButtons({...this.props})}
            <Grid container style={{marginTop: 20}}>
                <PicturesViewer pictures={pictures} cols={4}
                                onRemovePicture={(picture) => {
                                    onRemovePicture(picture)
                                }}
                                onSelectPicture={(picture) => onSelectPicture(picture)}
                >
                    {
                        (pictures.length > 0)
                            ? <GetPictureButton
                                onGetPictureSuccess={(dataURL) => {
                                    onAddPicture({
                                        uuid: article.uuid,
                                        dataURL: dataURL
                                    })
                                }}>
                                {(pictures.length > 0) ? (
                                    <div style={{textAlign: "center"}}>
                                        <CameraIcon color={"primary"}/>
                                        <Typography variant={"body1"} color={"primary"}>Voeg foto toe</Typography>
                                    </div>
                                ) : null}
                            </GetPictureButton> : null
                    }
                </PicturesViewer>
            </Grid>
        </CardContent>);
    }

    renderButtons({onAddPicture, article, dossier, match, pictures}) {
        if (!dossier.unloadingStartDate) {
            return null;
        }
        return (
            <Grid container justify={"flex-end"} style={{marginTop: 20}}>
                {
                    this.missingLocations(article) ? (
                        <Button variant={"outlined"}
                                color={"primary"}
                                onClick={this.openLocationDialog}>
                            <AddIcon/>&nbsp; Locatie
                        </Button>
                    ) : null
                }
                {
                    (article.damage.type === "DAMAGE") ? (
                        <Badge color="secondary" badgeContent={"!"} style={{marginTop: 10, marginRight: 8}}>
                            <Button style={{marginTop: 0, marginRight: 0}} variant={"outlined"}
                                    color={"primary"}
                                    onClick={this.openDamageDialog}>
                                <AddIcon/>&nbsp; Schade
                            </Button>
                        </Badge>
                    ) : (
                        <Button variant={"outlined"}
                                color={"primary"}
                                onClick={this.openDamageDialog}>
                            <AddIcon/>&nbsp; Schade
                        </Button>
                    )
                }

                {
                    (pictures.length <= 0)
                        ? (
                            <GetPictureButton
                                buttonText={"Foto"}
                                variant={"outlined"} color={"primary"} onGetPictureSuccess={(dataURL) => {
                                onAddPicture({
                                    uuid: article.uuid,
                                    dataURL: dataURL
                                })
                            }}/>
                        ) : null
                }

                <Button variant={"outlined"} color={"primary"} component={Link}
                        to={`${match.url}/article/${article.uuid}/line/add`}>
                    <AddIcon/>&nbsp; Regel
                </Button>

                <Button variant={"raised"} color={"primary"} onClick={this.handlePrepareFinish}>
                    <CheckIcon/>&nbsp; Finish
                </Button>

            </Grid>
        )
    }

    onSelectRowItem(event, selectedArticleLine) {
        const {history, match, article} = this.props;
        history.push(
            `${match.url}/article/${article.uuid}/line/${selectedArticleLine.uuid}/edit`
        );
    }
}

const mapStateToProps = ({dossiers, selectedArticleId, expandDossierHeader, selectedDossier}) => {
    const selectedArticle = findByArticleId(selectedDossier, selectedArticleId);
    return {
        dossier: selectedDossier,
        article: selectedArticle,
        expanded: !expandDossierHeader,
        pictures: selectedArticle ? selectedArticle.pictures : []
    }
};

const mapDispatchToProps = (dispatch) => ({
    onAddPicture: (payload) => dispatch(addPictureToArticle(payload)),
    onFinish: (payload) => {
        dispatch(finishUnloadArticle(payload));
        dispatch(doResetArticleSelection())
    },
    onRemovePicture: (payload) => {
        dispatch(removePictureFromArticle(payload))
    },
    onAddDamage: (payload) => {
        dispatch(addDamageToArticle(payload))
    },
    resetForm: (payload) => {
        dispatch(reset(payload));
    },
    onAddLocation: (payload) => {
        let article = payload.article;
        article.lines.forEach((line) => {
            line.location = payload.location;
            dispatch(editArticleLine({article: payload.article, newLine: line}))
        });

    },
});

const ArticleDetailCard = connect(mapStateToProps, mapDispatchToProps)(withRouter(_ArticleDetailCard));

export {ArticleDetailCard};