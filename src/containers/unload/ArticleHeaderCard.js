import React, { PureComponent } from 'react';
import {
    EmptyCardContent,
    ExpandableCard,
    LabelAndTextValue,
    InstructionText,
    MarksAndNumbers,
    MarksAndNumbersDialog
} from "../../components/common";
import { Grid } from "@material-ui/core/";
import {
    addNewMarksAndNumbersToArticle,
    doCollapseArticleHeader,
    doExpandArticleHeader,
    findByArticleId
} from "../../actions-reducers";
import { connect} from "react-redux";

class _ArticleHeaderCard extends PureComponent {

    constructor(props) {
        super(props);
        this.showMarksAndNumbersDialog = this.showMarksAndNumbersDialog.bind(this);
        this.hideMarksAndNumbersDialog = this.hideMarksAndNumbersDialog.bind(this);
        this.state = {
            showMarksAndNumbersDialog: false,
        };
    }

    showMarksAndNumbersDialog() {
        this.setState({showMarksAndNumbersDialog: true});
    }

    hideMarksAndNumbersDialog() {
        this.setState({showMarksAndNumbersDialog: false});
    }

    render() {
        const { article, expanded, onCollapse, onExpand, styles, onMarksAndNumbers } = this.props;

        return (
            <ExpandableCard
                title='Artikel Info'
                expanded = {expanded}
                onCollapse = {() => onCollapse()}
                onExpand = {() => onExpand()}
                styles={styles}
            >
                <MarksAndNumbersDialog
                    open={this.state.showMarksAndNumbersDialog}
                    onOk={(data) => {
                        this.hideMarksAndNumbersDialog();
                        onMarksAndNumbers(data)
                    }}
                    onCancel={this.hideMarksAndNumbersDialog}
                />
                {this.renderArticleHeader(article)}
            </ExpandableCard>
        );
    }

    renderArticleHeader(article) {
        if (!article) {
            return (
                <EmptyCardContent
                    imageSource="assets/pallet.png"
                    imageWidth={150}
                    label="Geen artikel geselecteerd"
                />)
        }
        const {editMarksAndNumbers} = this.props;
        const {marksAndNumbers, colli, packing, receiver, description, finalDestination, instructions, snsiNumber, newMarksAndNumbers} = article;

        return (
            <Grid container
                  direction={"row"}
            >
                <Grid item xs={6}>
                    <LabelAndTextValue label='SNSI Nr:' labelSize={4} value={snsiNumber}/>
                    <LabelAndTextValue label='Colli/verp.:' labelSize={4} value={colli + " " + packing}/>
                    <LabelAndTextValue label='Instructions:' labelSize={4} value={''}/>
                    <InstructionText value={instructions}/>
                </Grid>
                <Grid item xs={6}>
                    <LabelAndTextValue label='Omschr:' labelSize={4} value={description}/>
                    <MarksAndNumbers label='Marks & Nrs:' labelSize={4} value={marksAndNumbers} newValue={newMarksAndNumbers}
                                     onClick={() => this.showMarksAndNumbersDialog()}
                                     editMarksAndNumbers = { editMarksAndNumbers }/>
                    <LabelAndTextValue label='Ontvanger:' labelSize={4} value={receiver}/>
                    <LabelAndTextValue label='Final Dest.:' labelSize={4} value={finalDestination}/>
                </Grid>
            </Grid>
        );
    }
}

const mapStateToProps = ({dossiers, selectedArticleId, expandArticleHeader, selectedDossier}) => {
    const selectedArticle = findByArticleId(selectedDossier, selectedArticleId);
    return {
        article: selectedArticle,
        expanded: expandArticleHeader,
        editMarksAndNumbers: (selectedDossier != null && selectedDossier.unloadingStartDate != null)
    }
};

const mapDispatchToProps = (dispatch) => ({
    onCollapse: () => dispatch(doCollapseArticleHeader()),
    onExpand: () => dispatch(doExpandArticleHeader()),
    onMarksAndNumbers: (payload) => {
        dispatch(addNewMarksAndNumbersToArticle(payload))
    },
});

const ArticleHeaderCard = connect(mapStateToProps, mapDispatchToProps)(_ArticleHeaderCard);

export { ArticleHeaderCard };