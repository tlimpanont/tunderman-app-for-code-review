import React from "react";
import registerServiceWorker from './registerServiceWorker';
import {tundermanDefaultTheme} from "./themes";
import {MuiThemeProvider} from "@material-ui/core";
import {Provider} from "react-redux";
import App from "./App";
import * as ReactDOM from "react-dom";
import store from "./store";


const startApp = () => {
    ReactDOM.render(
        <MuiThemeProvider theme={tundermanDefaultTheme}>
            <Provider store={store}>
                <App/>
            </Provider>
        </MuiThemeProvider>,
        document.getElementById("root")
    );
    registerServiceWorker();
};

if (window.cordova) {
    document.addEventListener('deviceready', startApp, false);
} else {
    startApp();
}



