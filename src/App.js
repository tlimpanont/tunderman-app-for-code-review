import React, {Component} from "react";
import "normalize.css";
import "./App.css";
import {connect} from "react-redux";
import {HashRouter as Router, Route, Switch} from "react-router-dom";
import {withStyles, CircularProgress, Grid} from "@material-ui/core";
import {compose} from "recompose";
import {AppSnackbar} from "./components/common/AppSnackbar";
import {
    DischargeNewOverviewPage,
    DischargeDraftOverviewPage,
    DischargeHistoryOverviewPage,
    UnloadDetailPage
} from "./components/pages";

class App extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const {dossiers, loading} = this.props;

        return (
            <div>
                <AppSnackbar />

                {(loading) ? (
                    <Grid container direction={"row"} alignItems={"center"} justify={"center"} >
                        <CircularProgress color={"secondary"} size={60}/>
                    </Grid>
                ) : null}
                <div>
                    <Router>
                        <Switch>
                            <Route exact path={"/"} component={DischargeNewOverviewPage}/>
                            <Route path={"/history"} component={DischargeHistoryOverviewPage}/>
                            <Route path={"/draft"} component={DischargeDraftOverviewPage}/>
                            <Route path={"/unload/:dossierNumber"} component={UnloadDetailPage}/>
                        </Switch>
                    </Router>
                </div>
            </div>

        );
    }
}

export default compose(
    connect((state) => {
        return {
            dossiers: state.dossiers,
            loading: state.app.loading
        }
    })
)(App)
